<?php
Class Home extends CI_Controller{
    
    var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API_Gateway="http://localhost:9000";
        $this->API_Internal="http://localhost:9010";
        $this->API_Eksternal="http://localhost9020";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function index(){
        $this->load->view('Komponen/Header');
        $this->load->view('LandingPage/Home');
        $this->load->view('Komponen/footer');
    }

    function formPendaftaranKurir(){
        $ch = curl_init($this->API_Gateway.'/getBbm');
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data['kendaraan'] = json_decode($server_output);
        curl_close ($ch);
        // print_r($data['kendaraan']);die;

        $this->load->view('Komponen/Header');
        $this->load->view('LandingPage/PendaftaranKurir',$data);
        $this->load->view('Komponen/footer');
    }

    function formPendaftaranPelanggan(){
        $this->load->view('Komponen/Header');
        $this->load->view('LandingPage/PendaftaranPelanggan');
        $this->load->view('Komponen/footer');
    }

    function pendaftaranKurir(){
        $config['upload_path'] = './assets/FotoPendaftaran';
         $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto_ktp')){
            $result1 = $this->upload->data();
            $foto_ktp = $result1['file_name'];
         }
         if ($this->upload->do_upload('foto_npwp')){
            $result2 = $this->upload->data();
            $foto_npwp = $result2['file_name'];
         }
         if ($this->upload->do_upload('foto_diri')){
            $result3 = $this->upload->data();
            $foto_diri = $result3['file_name'];
         }
         if ($this->upload->do_upload('foto_stnk')){
            $result4 = $this->upload->data();
            $foto_stnk = $result4['file_name'];
         }
         if ($this->upload->do_upload('foto_sim')){
            $result5 = $this->upload->data();
            $foto_sim = $result5['file_name'];
         }

        $data = array(
            'email' => $this->input->post('email'), 
            'password' => $this->input->post('password'), 
            'nama_lengkap' => $this->input->post('nama_lengkap'), 
            'tempat_lahir' => $this->input->post('tempat_lahir'), 
            'tanggal_lahir' => $this->input->post('tanggal_lahir'), 
            'alamat' => $this->input->post('alamat'), 
            'nik' => $this->input->post('nik'), 
            'no_telp' => $this->input->post('no_telp'), 
            'foto_ktp' => $foto_ktp, 
            'foto_npwp' => $foto_npwp, 
            'foto_diri' => $foto_diri,

            'nama_kendaraan' => $this->input->post('nama_kendaraan'), 
            'plat_kendaraan' => $this->input->post('plat_kendaraan'), 
            'jenis_kendaraan' => $this->input->post('jenis_kendaraan'), 
            'tahun_kendaraan' => $this->input->post('tahun_kendaraan'), 
            'domisili' => $this->input->post('domisili'), 
            'id_bbm' => $this->input->post('id_bbm'), 
            'foto_stnk' => $foto_stnk,
            'foto_sim' => $foto_sim,
            'status_ketersediaan' => 'Available'
        );

        // print_r($data);die;

        $daftar_mitra = json_decode($this->curl->simple_post($this->API_Gateway.'/registrasiMitra', $data,  array(CURLOPT_BUFFERSIZE => 10)));
        // print_r($daftar_mitra);die;
        if ($daftar_mitra) {
            // Konfigurasi email
                $config = [
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8',
                    'protocol'  => 'smtp',
                    'smtp_host' => 'smtp.gmail.com',
                    'smtp_user' => 'frhnmln20@gmail.com',  // Email gmail
                    'smtp_pass'   => 'simpansandi',  // Password gmail
                    'smtp_crypto' => 'ssl',
                    'smtp_port'   => 465,
                    'crlf'    => "\r\n",
                    'newline' => "\r\n"
                ];

                $pesan = 'Untuk aktivasi akun sahabat partic mu sebagai kurir, silahkan klik link yang sudah diberikan dibawah ini <br>
                            http://localhost/partic-external/index.php/home/aktivasiAkun/'.$daftar_mitra->DataUser->id;
                $penerima = $daftar_mitra->DataUser->email;
                            // print_r($pesan);die;

                // Load library email dan konfigurasinya
                $this->load->library('email', $config);

                // Email dan nama pengirim
                $this->email->from('frhnmln20', 'Sahabat Partic');

                // Email penerima
                $this->email->to($penerima); // Ganti dengan email tujuan

                // Lampiran email, isi dengan url/path file
                // $this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

                // Subject email
                $this->email->subject('Aktivasi Akun Partic');

                // Isi email
                $this->email->message($pesan);

                // Tampilkan pesan sukses atau error
                if ($this->email->send()) {
                    echo 'Pendaftaran Sukses! Silahkan Aktivasi Email Terlebih Dahulu Sebelum Login.';
                } else {
                    echo 'Error! Pendaftaran Tidak Dapat Dilakukan.';
                }
        }
            redirect('home');
        // }else{
        //     echo "<script>alert('Gagal melakukan Pendaftaran!');history.go(-1);</script>";
        // }

    }

    function aktivasiAkun($id){
        // print_r($id);die;
        $ch = curl_init('http://localhost:9000/aktivasiAkunEksternal/'.$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        // curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($updatePengajuan));
        $server_output = curl_exec($ch);
        $data = json_decode($server_output);
        curl_close ($ch);
        // print_r($data->status);die;
        redirect('home');

        if ($data->status == 200) {
            echo "<script>alert('Aktivasi Akun Berhasil!');</script>";
            redirect('home');
        }else{
            echo "<script>alert('Aktivasi Akun Gagal!');</script>";
            redirect('home');
        }
    }

    function pendaftaranPelanggan(){
         $config['upload_path'] = './assets/FotoPendaftaran';
         $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto_ktp')){
            $result1 = $this->upload->data();
            $foto_ktp = $result1['file_name'];
         }
         if ($this->upload->do_upload('foto_npwp')){
            $result2 = $this->upload->data();
            $foto_npwp = $result2['file_name'];
         }
         if ($this->upload->do_upload('foto_diri')){
            $result3 = $this->upload->data();
            $foto_diri = $result3['file_name'];
         }

        $data = array(
            'email' => $this->input->post('email'), 
            'password' => $this->input->post('password'), 
            'nama_lengkap' => $this->input->post('nama_lengkap'), 
            'tempat_lahir' => $this->input->post('tempat_lahir'), 
            'tanggal_lahir' => $this->input->post('tanggal_lahir'), 
            'alamat' => $this->input->post('alamat'), 
            'nik' => $this->input->post('nik'), 
            'no_telp' => $this->input->post('no_telp'), 
            'foto_ktp' => $foto_ktp, 
            'foto_npwp' => $foto_npwp, 
            'foto_diri' => $foto_diri 
        );
        // print_r($data);die;

        $daftar_member = json_decode($this->curl->simple_post($this->API_Gateway.'/registrasiMember', $data,  array(CURLOPT_BUFFERSIZE => 10)));
        // print_r($daftar_member);die;
        if ($daftar_member) {
            // Konfigurasi email
                $config = [
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8',
                    'protocol'  => 'smtp',
                    'smtp_host' => 'smtp.gmail.com',
                    'smtp_user' => 'frhnmln20@gmail.com',  // Email gmail
                    'smtp_pass'   => 'simpansandi',  // Password gmail
                    'smtp_crypto' => 'ssl',
                    'smtp_port'   => 465,
                    'crlf'    => "\r\n",
                    'newline' => "\r\n"
                ];

                $pesan = 'Untuk aktivasi akun sahabat partic mu sebagai kurir, silahkan klik link yang sudah diberikan dibawah ini <br>
                            http://localhost/partic-external/index.php/home/aktivasiAkun/'.$daftar_member->DataUser->id;
                $penerima = $daftar_member->DataUser->email;
                            // print_r($penerima);die;

                // Load library email dan konfigurasinya
                $this->load->library('email', $config);

                // Email dan nama pengirim
                $this->email->from('frhnmln20', 'Sahabat Partic');

                // Email penerima
                $this->email->to($penerima); // Ganti dengan email tujuan

                // Lampiran email, isi dengan url/path file
                // $this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

                // Subject email
                $this->email->subject('Aktivasi Akun Partic');

                // Isi email
                $this->email->message($pesan);

                // Tampilkan pesan sukses atau error
                if ($this->email->send()) {
                    echo 'Pendaftaran Sukses! Silahkan Aktivasi Email Terlebih Dahulu Sebelum Login.';
                } else {
                    echo 'Error! Pendaftaran Tidak Dapat Dilakukan.';
                }
        }
            redirect('home');
        // }else{
        //     echo "<script>alert('Gagal melakukan Pendaftaran!');history.go(-1);</script>";
        // }
    }

    function cekResi(){
        $this->load->view('Komponen/Header');
        $this->load->view('LandingPage/CekResi');
        $this->load->view('Komponen/footer');
    }

    function hasilResi(){
        $no_resi = $this->input->post('no_resi');
        // print_r($no_resi);die;  

        $ch = curl_init('http://localhost:9000/cekResiStatus/'.$no_resi);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $server_output = curl_exec($ch);
        $data['status'] = json_decode($server_output);
        curl_close ($ch);

        $ch2 = curl_init('http://localhost:9000/cekResiKeterangan/'.$no_resi);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
        $server_output2 = curl_exec($ch2);
        $data['keterangan'] = json_decode($server_output2);
        // print_r($data['keterangan']);die;
        curl_close ($ch2);
        $this->load->view('Komponen/Header');
        $this->load->view('LandingPage/HasilResi',$data);
        $this->load->view('Komponen/footer');
    }

    function login(){ 
        $where = array(
               'email' => $this->input->post('email'),
               'password' => $this->input->post('password')
            );
        $email = array('email' => $this->input->post('email'));
        $login = $this->curl->simple_post($this->API_Gateway.'/login',$where);
        $resp = json_decode($login);
        if (isset($resp->code))
        {
            echo "<script>alert('Username atau Password salah!, Silahkan ulangi Login.');history.go(-2);</script>";
        } else {
            $token = 'Authorization: Bearer '.$resp->access_token;
            $ch = curl_init($this->API_Gateway.'/profilEksternal');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($token));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($email));

            $server_output = curl_exec($ch);
            $response = json_decode($server_output);
            // print_r($response);die;

            curl_close ($ch);

            if ($response->DataUser->role == 'mitra') {
                    $ch2 = curl_init($this->API_Gateway.'/profilMitra');
                    curl_setopt($ch2, CURLOPT_HTTPHEADER, array($token));
                    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch2, CURLOPT_POSTFIELDS,http_build_query($email));

                    $server_output2 = curl_exec($ch2);
                    $response2 = json_decode($server_output2);
                    // print_r($response2);die;
                    curl_close ($ch2);

                if ($response->DataUser->verifikasi == 2) {
                    $sess_data['token'] = $token;
                    $sess_data['id_mitra'] = $response2->DataMitra->id;
                    $sess_data['id_users'] = $response->DataUser->id;
                    $session = $this->session->set_userdata($sess_data);
                    redirect('mitra/belumVerifikasi');
                }else{
                    $sess_data['id_users'] = $response->DataUser->id;
                    $sess_data['email'] = $response->DataUser->email;
                    $sess_data['role'] = $response->DataUser->role;
                    $sess_data['id_biodata'] = $response->DataBiodata->id;
                    $sess_data['nama_lengkap'] = $response->DataBiodata->nama_lengkap;
                    $sess_data['tanggal_lahir'] = $response->DataBiodata->tanggal_lahir;
                    $sess_data['tempat_lahir'] = $response->DataBiodata->tempat_lahir;
                    $sess_data['alamat'] = $response->DataBiodata->alamat;
                    $sess_data['no_telp'] = $response->DataBiodata->no_telp;
                    $sess_data['foto_ktp'] = $response->DataBiodata->foto_ktp;
                    $sess_data['foto_npwp'] = $response->DataBiodata->foto_npwp;
                    $sess_data['foto_diri'] = $response->DataBiodata->foto_diri;
                    $sess_data['token'] = $token;
                    $sess_data['id_mitra'] = $response2->DataMitra->id;
                    $session = $this->session->set_userdata($sess_data);
                    // print_r($sess_data);die;

                    redirect('Mitra/daftarMitra');
                }
            }else{
                    if ($response->DataUser->verifikasi == 2) {
                    $sess_data['token'] = $token;
                    $sess_data['id_users'] = $response->DataUser->id;
                    $session = $this->session->set_userdata($sess_data);
                    redirect('member/belumVerifikasi');
                }else{
                    $sess_data['id_users'] = $response->DataUser->id;
                    $sess_data['email'] = $response->DataUser->email;
                    $sess_data['role'] = $response->DataUser->role;
                    $sess_data['id_biodata'] = $response->DataBiodata->id;
                    $sess_data['nama_lengkap'] = $response->DataBiodata->nama_lengkap;
                    $sess_data['tanggal_lahir'] = $response->DataBiodata->tanggal_lahir;
                    $sess_data['tempat_lahir'] = $response->DataBiodata->tempat_lahir;
                    $sess_data['alamat'] = $response->DataBiodata->alamat;
                    $sess_data['no_telp'] = $response->DataBiodata->no_telp;
                    $sess_data['foto_ktp'] = $response->DataBiodata->foto_ktp;
                    $sess_data['foto_npwp'] = $response->DataBiodata->foto_npwp;
                    $sess_data['foto_diri'] = $response->DataBiodata->foto_diri;
                    $sess_data['token'] = $token;
                    $session = $this->session->set_userdata($sess_data);
                    // print_r($sess_data);die;

                    redirect('Member/profil');
                }
            } 
        }
    }

}