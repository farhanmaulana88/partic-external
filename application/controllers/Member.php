<?php
Class Member extends CI_Controller{
    
    var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API_Gateway="http://localhost:9000";
        $this->API_Internal="http://localhost:9010";
        $this->API_Eksternal="http://localhost9020";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');

        if($this->session->userdata('token') == "" and
            $this->session->userdata('id_users') == ""){
            redirect(base_url("index.php/Home"));
            }
    }

    function daftarMember(){
        $ch = curl_init($this->API_Gateway.'/member/getNamaPerusahaan');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data['nama_perusahaan'] = json_decode($server_output);
        curl_close ($ch);
        // print_r($server_output); die;

        $ch2 = curl_init($this->API_Gateway.'/member/getMemberByIdUsers/'.$this->session->userdata('id_users'));
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $server_output2 = curl_exec($ch2);
        $data['daftar_member'] = json_decode($server_output2);
        curl_close ($ch2);
        // print_r($data['daftar_member']); die;
        $this->load->view('KomponenMember/Header');
        $this->load->view('KomponenMember/Sidebar');
        $this->load->view('Member/DaftarMember', $data);
        $this->load->view('KomponenMember/Footer');
    }

    function getDataCabang($id_perusahaan){
        // print_r($id_perusahaan);die;
        $ch = curl_init($this->API_Gateway.'/member/getCabangByIdPerusahaan/'.$id_perusahaan);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        // print_r($server_output);die;
        $data['cabang_perusahaan'] = json_decode($server_output);
        curl_close ($ch);

        $this->load->view('KomponenMember/Header');
        $this->load->view('KomponenMember/Sidebar');
        $this->load->view('Member/CabangPerusahaan', $data);
        $this->load->view('KomponenMember/Footer');
    }

    function riwayatPengiriman(){
        $ch = curl_init($this->API_Gateway.'/member/getRiwayatPengirimanByIdUsers/'.$this->session->userdata('id_users'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        // print_r($server_output);die;
        $data['riwayat'] = json_decode($server_output);
        curl_close ($ch);

        $this->load->view('KomponenMember/Header');
        $this->load->view('KomponenMember/Sidebar');
        $this->load->view('Member/RiwayatPengiriman', $data);
        $this->load->view('KomponenMember/Footer');
    }

    function statusPengiriman($no_resi){
        $ch = curl_init('http://localhost:9000/cekResiStatus/'.$no_resi);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $server_output = curl_exec($ch);
        $data['status'] = json_decode($server_output);
        curl_close ($ch);

        $ch2 = curl_init('http://localhost:9000/cekResiKeterangan/'.$no_resi);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
        $server_output2 = curl_exec($ch2);
        $data['keterangan'] = json_decode($server_output2);
        // print_r($data['keterangan']);die;
        curl_close ($ch2);
        $this->load->view('KomponenMember/Header');
        $this->load->view('KomponenMember/Sidebar');
        $this->load->view('Member/StatusPengiriman', $data);
        $this->load->view('KomponenMember/Footer');
    }

    function daftarPengiriman($id){
        // print_r($id); die;
        $ch = curl_init($this->API_Gateway.'/member/getPengirimanByIdUsers/'.$id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        // print_r($server_output);die;
        $data['daftar_pengiriman'] = json_decode($server_output);
        curl_close ($ch);
        // print_r($server_output); die;
        $this->load->view('KomponenMember/Header');
        $this->load->view('KomponenMember/Sidebar');
        $this->load->view('Member/Pengiriman',$data);
        $this->load->view('KomponenMember/Footer');
    }
    
    function belumVerifikasi(){
        $this->load->view('Member/BelumVerifikasi');
    }

    function pengajuanBermember(){
        $id = array(
            'id_perusahaan' => $this->input->post('id_perusahaan'),
            'id_user' => $this->session->userdata('id_users')
        );
        
        // print_r($id); die;
        $ch = curl_init('http://localhost:9000/member/pengajuanMember');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($id));
        $server_output = curl_exec($ch);
        $data = json_decode($server_output);
        // print_r($data);die;
        curl_close ($ch);

        if ($server_output) {
            echo "<script>alert('Berhasil melakukan Pendaftaran!');history.go(-1);</script>";
        }else{
            echo "<script>alert('Gagal melakukan Pendaftaran!');history.go(-1);</script>";
        }
    }

    function updatePengajuanTarif(){
        if ($this->input->post('minimal_berat') == '') {
            $minimal_berat = 0;
        }else{
            $minimal_berat = $this->input->post('minimal_berat');
        }

        if ($this->input->post('maksimal_berat') == '') {
            $maksimal_berat = 0;
        }else{
            $maksimal_berat = $this->input->post('maksimal_berat');
        }

        if ($this->input->post('minimal_jumlah') == '') {
            $minimal_jumlah = 0;
        }else{
            $minimal_jumlah = $this->input->post('minimal_jumlah');
        }

        if ($this->input->post('maksimal_jumlah') == '') {
            $maksimal_jumlah = 0;
        }else{
            $maksimal_jumlah = $this->input->post('maksimal_jumlah');
        }

        if ($this->input->post('minimal_dimensi') == '') {
            $minimal_dimensi = 0;
        }else{
            $minimal_dimensi = $this->input->post('minimal_dimensi');
        }

        if ($this->input->post('maksimal_dimensi') == '') {
            $maksimal_dimensi = 0;
        }else{
            $maksimal_dimensi = $this->input->post('maksimal_dimensi');
        }

        $updatePengajuan = array(
            'id' => $this->input->post('id'), 
            'id_perusahaan' => $this->input->post('id_perusahaan'), 
            'id_mitra' => $this->session->userdata('id_mitra'), 
            'asal' => $this->input->post('asal'), 
            'tujuan' => $this->input->post('tujuan'), 
            'jenis_pengiriman' => $this->input->post('jenis_pengiriman'), 
            'moda' => $this->input->post('moda'), 
            'jenis_pengangkutan' => $this->input->post('jenis_pengangkutan'), 
            'nama_barang' => $this->input->post('nama_barang'), 
            'tarif_normal' => $this->input->post('tarif_normal'), 
            'durasi' => $this->input->post('durasi'), 
            'minimal_berat' => $minimal_berat,
            'maksimal_berat' => $maksimal_berat, 
            'minimal_jumlah' => $minimal_jumlah, 
            'maksimal_jumlah' => $maksimal_jumlah, 
            'minimal_dimensi' => $minimal_dimensi, 
            'maksimal_dimensi' => $maksimal_dimensi
        );
        
        // print_r($updatePengajuan); die;
            $ch = curl_init('http://localhost:9000/mitra/updatePengajuanTarif/'.$this->input->post('id'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($updatePengajuan));
            $server_output = curl_exec($ch);
            $data = json_decode($server_output);
            print_r($data);die;
            curl_close ($ch);
        if ($server_output) {
            echo "<script>alert('Berhasil melakukan Pendaftaran!');history.go(-1);</script>";
        }else{
            echo "<script>alert('Gagal melakukan Pendaftaran!');history.go(-1);</script>";
        }
    }

    function deletePengajuanTarif($id){
        // print_r($id); die;
            $ch = curl_init('http://localhost:9000/mitra/deletePengajuanTarif/'.$id);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            $server_output = curl_exec($ch);
            curl_close ($ch);
            // print_r($server_output); die;
        if ($server_output) {
            echo "<script>alert('Berhasil melakukan Pendaftaran!');history.go(-1);</script>";
        }else{
            echo "<script>alert('Gagal melakukan Pendaftaran!');history.go(-1);</script>";
        }
    }

    function profil(){
        $ch = curl_init($this->API_Gateway.'/showProfil/'.$this->session->userdata('id_users'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data['profil'] = json_decode($server_output);
        curl_close ($ch);

        $this->load->view('KomponenMember/Header');
        $this->load->view('KomponenMember/Sidebar');
        $this->load->view('Member/profil',$data);
        $this->load->view('KomponenMember/Footer');
    }

    public function Logout()
    {
        $this->session->sess_destroy();
        redirect(base_url("index.php/Home"));
    }

    function updateProfil(){
        $config['upload_path'] = './assets/FotoPendaftaran';
         $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto_diri')){
            $result1 = $this->upload->data();
            $foto_diri = $result1['file_name'];
         }else{
            $foto_diri = '-';
         }
         if ($this->upload->do_upload('foto_ktp')){
            $result2 = $this->upload->data();
            $foto_ktp = $result2['file_name'];
         }else{
            $foto_ktp = '-';
         }
         if ($this->upload->do_upload('foto_npwp')){
            $result3 = $this->upload->data();
            $foto_npwp = $result3['file_name'];
         }else{
            $foto_npwp = '-';
         }

        $dataUpdate = array(
            'email' => $this->input->post('email'), 
            'nama_lengkap' => $this->input->post('nama_lengkap'), 
            'tanggal_lahir' => $this->input->post('tanggal_lahir'), 
            'tempat_lahir' => $this->input->post('tempat_lahir'), 
            'alamat' => $this->input->post('alamat'), 
            'no_telp' => $this->input->post('no_telp'), 
            'nik' => $this->input->post('nik'), 
            'foto_diri' => $foto_diri, 
            'foto_ktp' => $foto_ktp, 
            'foto_npwp' => $foto_npwp
        );

        // var_dump($dataUpdate);die;

            $ch = curl_init($this->API_Gateway.'/updateProfilMitra/'.$this->session->userdata('id_users'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($dataUpdate));
            $server_output = curl_exec($ch);
            $profil = json_decode($server_output);
            // var_dump($profil);die;
            curl_close ($ch);

        redirect('member/profil');
    }

}