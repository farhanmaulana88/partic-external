<?php
Class Mitra extends CI_Controller{
    
    var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API_Gateway="http://localhost:9000";
        $this->API_Internal="http://localhost:9010";
        $this->API_Eksternal="http://localhost9020";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');

        if($this->session->userdata('token') == "" and
            $this->session->userdata('id_mitra') == "" and
            $this->session->userdata('id_users') == ""){
            redirect(base_url("index.php/Home"));
            }
    }

    function pickUp(){
        $ch = curl_init($this->API_Gateway.'/mitra/getLobiMitraByIdMitra/'.$this->session->userdata('id_mitra'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data['daftar_lobi_mitra'] = json_decode($server_output);
        curl_close ($ch);

        $this->load->view('KomponenMitra/Header');
        $this->load->view('KomponenMitra/Sidebar');
        $this->load->view('Mitra/PickUp',$data);
        $this->load->view('KomponenMitra/Footer');
    }


    function DaftarPengiriman($id,$longlat){
        $nn =[];
        $titikAwal = $longlat;
        $this->session->set_userdata('titikAwal',$titikAwal);
        $waktu = [];
        $jarak = [];
        $longlatArr = [];

        $ch = curl_init($this->API_Gateway.'/mitra/getPengirimanPickUpByIdKelompokPengiriman/'.$id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data = json_decode($server_output);
        // var_dump($data);die;
        $data2 = json_decode($server_output);
        $data3 = json_decode($server_output);
        $data4 = json_decode($server_output);
        curl_close ($ch);

        foreach ($data as $wa) {
            if ($nn == null) {
                $longlat = "";
                foreach ($data2 as $key) {
                    $longlat .= $key->latitude.'%2C'.$key->longitude.'%7C';
                }

                $hitGmaps = curl_init('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$this->session->userdata('titikAwal').'&destinations='.$longlat.'&key=AIzaSyCgP-WOSIf3GPWcFmPB1IXVMIZPiebhOno');
                curl_setopt($hitGmaps, CURLOPT_HTTPHEADER, array('Accept:application/json'));
                curl_setopt($hitGmaps, CURLOPT_RETURNTRANSFER, 1);
                $server_output2 = curl_exec($hitGmaps);
                $tahap1 = json_decode($server_output2);

                $resp = [];
                $idxPengiriman = 0;
                foreach ($data3 as $aw) {
                    $el = [
                        'id_pengiriman' => $aw->id_pengiriman,
                        'value_jarak' => $tahap1->rows[0]->elements[$idxPengiriman]->distance->value,
                        'value_waktu' => $tahap1->rows[0]->elements[$idxPengiriman]->duration->value,
                        'latlong' => $aw->latitude.','.$aw->longitude
                    ];

                    array_push($resp, $el);
                    $idxPengiriman++;
                }

                usort($resp, function($a, $b) {
                return $a['value_jarak'] <=> $b['value_jarak'];
                return $a['value_waktu'] <=> $b['value_waktu'];
                });

                array_push($waktu, $resp[0]['value_waktu']);
                array_push($jarak, $resp[0]['value_jarak']);
                array_push($nn, $resp[0]['id_pengiriman']);
                array_push($longlatArr, $resp[0]['latlong']);

                foreach($data4 as $foo)
                {
                    if ($foo->id_pengiriman == $resp[0]['id_pengiriman'])
                    {
                        $titikBaru = $foo->latitude.','.$foo->longitude;
                    }
                }
                $this->session->set_userdata('titikAwal', $titikBaru);
                
            } else {
                $longlat = "";
                $dataFinal = [];

                foreach ($data as $key) {
                    if (!in_array($key->id_pengiriman, $nn, true)) {
                        $longlat .= $key->latitude.'%2C'.$key->longitude.'%7C';
                        array_push($dataFinal, $key);
                    }
                }

                $hitGmaps = curl_init('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$this->session->userdata('titikAwal').'&destinations='.$longlat.'&key=AIzaSyCgP-WOSIf3GPWcFmPB1IXVMIZPiebhOno');
                curl_setopt($hitGmaps, CURLOPT_HTTPHEADER, array('Accept:application/json'));
                curl_setopt($hitGmaps, CURLOPT_RETURNTRANSFER, 1);
                $server_output2 = curl_exec($hitGmaps);
                $tahap1 = json_decode($server_output2);

                $resp = [];
                $idxPengiriman = 0;

                foreach ($dataFinal as $aw) {
                    $el = [
                        'id_pengiriman' => $aw->id_pengiriman,
                        'value_jarak' => $tahap1->rows[0]->elements[$idxPengiriman]->distance->value,
                        'value_waktu' => $tahap1->rows[0]->elements[$idxPengiriman]->duration->value,
                        'latlong' => $aw->latitude.','.$aw->longitude
                    ];

                    array_push($resp, $el);
                    $idxPengiriman++;
                }

                usort($resp, function($a, $b) {
                return $a['value_jarak'] <=> $b['value_jarak'];
                return $a['value_waktu'] <=> $b['value_waktu'];
                });
                // var_dump($resp);die;

                array_push($waktu, $resp[0]['value_waktu']);
                array_push($jarak, $resp[0]['value_jarak']);
                array_push($nn, $resp[0]['id_pengiriman']);
                array_push($longlatArr, $resp[0]['latlong']);

                foreach($data4 as $foo)
                {
                    if ($foo->id_pengiriman == $resp[0]['id_pengiriman'])
                    {
                        $titikBaru = $foo->latitude.','.$foo->longitude;
                    }
                }
                $this->session->set_userdata('titikAwal', $titikBaru);
            }
        }
        // print_r($longlatArr);die;

        $finalWaypoints = '';

        $desti = array_values(array_slice($longlatArr, -1))[0];
        $slice = array_pop($longlatArr);

        $lastElement = end($longlatArr);
        foreach ($longlatArr as $key) {
            if ($key != $lastElement) {
            $finalWaypoints .= $key.'|';
            }else{
            $finalWaypoints .= $key;
            }
        }
        // print_r($desti);die;

        $totalWaktu = 0;
        $totalJarak = 0;
        $finalResp = [];

        foreach ($waktu as $waktuFinal) {
            $totalWaktu += $waktuFinal;
        }

        foreach ($jarak as $jarakFinal) {
            $totalJarak += $jarakFinal;
        }
        // print_r($totalWaktu);die;

        $totalJarakAkhir = $totalJarak/1000;
        $totalWaktuAkhir = $totalWaktu/60;

        foreach ($nn as $nnFinal) {
            foreach ($data4 as $dataFinal) {
                if ($dataFinal->id_pengiriman == $nnFinal) {
                    array_push($finalResp, $dataFinal);
                }
            }
        }

        $ch2 = curl_init($this->API_Gateway.'/mitra/getKendaraan/'.$this->session->userdata('id_mitra'));
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $server_output3 = curl_exec($ch2);
        $kendaraan = json_decode($server_output3);
        curl_close ($ch2);

        $ch3 = curl_init($this->API_Gateway.'/mitra/getBbmByIdBbm/'.$kendaraan[0]->id_bbm);
        curl_setopt($ch3, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
        $server_output4 = curl_exec($ch3);
        $bbm = json_decode($server_output4);

        curl_close ($ch3);

        if ($kendaraan[0]->jenis_kendaraan == 'mobil') {
            $liter = $totalJarakAkhir/14.8;
            $totalBiayaAkhir = $liter*$bbm->harga;
        }else{
            $liter = $totalJarakAkhir/5.7;
            $totalBiayaAkhir = $liter*$bbm->harga;
        }


        $this->load->view('KomponenMitra/Header');
        $this->load->view('KomponenMitra/Sidebar');
        $this->load->view('Mitra/Pengiriman', compact('finalResp','totalWaktuAkhir','totalJarakAkhir','totalBiayaAkhir','desti','finalWaypoints'));
        $this->load->view('KomponenMitra/Footer');
    }

    function DaftarPengirimanDesc($id,$longlat){
        // print_r('aw');die;
        $nn =[];
        $nn2 =[];
        $titikAwal = $longlat;
        $this->session->set_userdata('titikAwalDesc',$titikAwal);
        $waktu = [];
        $jarak = [];
        $waktu2 = [];
        $jarak2 = [];
        $longlatArr = [];
        $longlatArr2 = [];

        $ch = curl_init($this->API_Gateway.'/mitra/getPengirimanPickUpByIdKelompokPengiriman/'.$id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data = json_decode($server_output);
        // var_dump($data);die;
        $data2 = json_decode($server_output);
        $data3 = json_decode($server_output);
        $data4 = json_decode($server_output);
        $data5 = json_decode($server_output);
        $data6 = json_decode($server_output);
        curl_close ($ch);
        // print_r($this->session->userdata('titikAwalDesc'));die;
        $longlat2 = '';
        $terjauh = [];
        foreach ($data5 as $key) {
                    $longlat2 .= $key->latitude.'%2C'.$key->longitude.'%7C';
                    // array_push($test, $longilat);
                }
                    // print_r($longlat);die;
            $hitGmaps2 = curl_init('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$this->session->userdata('titikAwalDesc').'&destinations='.$longlat2.'&key=AIzaSyCgP-WOSIf3GPWcFmPB1IXVMIZPiebhOno');
                curl_setopt($hitGmaps2, CURLOPT_HTTPHEADER, array('Accept:application/json'));
                curl_setopt($hitGmaps2, CURLOPT_RETURNTRANSFER, 1);
                $server_output3 = curl_exec($hitGmaps2);
                $tahap2 = json_decode($server_output3);
                // print_r($tahap2);die;

                $respJauh = [];
                $jauh = [];
                $idxPengirimanJauh = 0;
                foreach ($data6 as $aw) {
                    $ell = [
                        'id_pengiriman' => $aw->id_pengiriman,
                        'value_jarak' => $tahap2->rows[0]->elements[$idxPengirimanJauh]->distance->value,
                        'value_waktu' => $tahap2->rows[0]->elements[$idxPengirimanJauh]->duration->value,
                        'latitude' => $aw->latitude,
                        'longitude' => $aw->longitude
                    ];

                    array_push($respJauh, $ell);
                    $idxPengirimanJauh++;
                }

                usort($respJauh, function($a, $b) {
                    if($a['value_jarak']==$b['value_jarak']) return 0;
                    return $a['value_jarak'] < $b['value_jarak']?1:-1;
                });
                // var_dump($respJauh);die;
                $start = $respJauh[0]['latitude'].','.$respJauh[0]['longitude'];
                $waktuDesc = $respJauh[0]['value_waktu'];
                $jarakDesc = $respJauh[0]['value_jarak'];
                array_push($waktu2, $respJauh[0]['value_waktu']);
                array_push($jarak2, $respJauh[0]['value_jarak']);
                array_push($nn2, $respJauh[0]['id_pengiriman']);
                array_push($longlatArr2, $start);
                $nn22 = $respJauh[0]['id_pengiriman'];
                // print_r($respJauh);die;
                $this->session->set_userdata('titikAwalDesc', $start);
        foreach ($data as $wa) {
            if ($nn == null) {
                    
                $longlat = "";
                foreach ($data2 as $key) {
                    $longlat .= $key->latitude.'%2C'.$key->longitude.'%7C';
                }
                // $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$this->session->userdata('titikAwalDesc').'&destinations='.$longlat.'&key=AIzaSyCgP-WOSIf3GPWcFmPB1IXVMIZPiebhOno';
                // print_r($url);die;

                $hitGmaps = curl_init('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$this->session->userdata('titikAwalDesc').'&destinations='.$longlat.'&key=AIzaSyCgP-WOSIf3GPWcFmPB1IXVMIZPiebhOno');
                curl_setopt($hitGmaps, CURLOPT_HTTPHEADER, array('Accept:application/json'));
                curl_setopt($hitGmaps, CURLOPT_RETURNTRANSFER, 1);
                $server_output2 = curl_exec($hitGmaps);
                $tahap1 = json_decode($server_output2);

                $resp = [];
                $idxPengiriman = 0;
                foreach ($data3 as $aw) {

                    $el = [
                        'id_pengiriman' => $aw->id_pengiriman,
                        'value_jarak' => $tahap1->rows[0]->elements[$idxPengiriman]->distance->value,
                        'value_waktu' => $tahap1->rows[0]->elements[$idxPengiriman]->duration->value,
                        'latlong' => $aw->latitude.','.$aw->longitude,
                    ];

                    array_push($resp, $el);
                    $idxPengiriman++;
                }

                usort($resp, function($a, $b) {
                return $a['value_jarak'] <=> $b['value_jarak'];
                return $a['value_waktu'] <=> $b['value_waktu'];
                });

                // print_r($resp);die;
                array_push($waktu, $resp[0]['value_waktu']);
                array_push($jarak, $resp[0]['value_jarak']);
                array_push($nn, $resp[0]['id_pengiriman']);
                array_push($longlatArr, $resp[0]['latlong']);

                foreach($data4 as $foo)
                {
                    if ($foo->id_pengiriman == $resp[0]['id_pengiriman'])
                    {
                        $titikBaru = $foo->latitude.','.$foo->longitude;
                    }
                }
                $this->session->set_userdata('titikAwalDesc', $titikBaru);
                
            } else {
                $longlat = "";
                $dataFinal = [];

                foreach ($data as $key) {
                    if (!in_array($key->id_pengiriman, $nn, true)) {
                        $longlat .= $key->latitude.'%2C'.$key->longitude.'%7C';
                        array_push($dataFinal, $key);
                    }
                }

                $hitGmaps = curl_init('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$this->session->userdata('titikAwalDesc').'&destinations='.$longlat.'&key=AIzaSyCgP-WOSIf3GPWcFmPB1IXVMIZPiebhOno');
                curl_setopt($hitGmaps, CURLOPT_HTTPHEADER, array('Accept:application/json'));
                curl_setopt($hitGmaps, CURLOPT_RETURNTRANSFER, 1);
                $server_output2 = curl_exec($hitGmaps);
                $tahap1 = json_decode($server_output2);

                $resp = [];
                $idxPengiriman = 0;

                foreach ($dataFinal as $aw) {
                    $el = [
                        'id_pengiriman' => $aw->id_pengiriman,
                        'value_jarak' => $tahap1->rows[0]->elements[$idxPengiriman]->distance->value,
                        'value_waktu' => $tahap1->rows[0]->elements[$idxPengiriman]->duration->value,
                        'latlong' => $aw->latitude.','.$aw->longitude
                    ];

                    array_push($resp, $el);
                    $idxPengiriman++;
                }

                usort($resp, function($a, $b) {
                return $a['value_jarak'] <=> $b['value_jarak'];
                return $a['value_waktu'] <=> $b['value_waktu'];
                });
                // var_dump($resp);die;

                array_push($waktu, $resp[0]['value_waktu']);
                array_push($jarak, $resp[0]['value_jarak']);
                array_push($nn, $resp[0]['id_pengiriman']);
                array_push($longlatArr, $resp[0]['latlong']);

                foreach($data4 as $foo)
                {
                    if ($foo->id_pengiriman == $resp[0]['id_pengiriman'])
                    {
                        $titikBaru = $foo->latitude.','.$foo->longitude;
                    }
                }
                $this->session->set_userdata('titikAwalDesc', $titikBaru);
            }
            
                    }
        $nnMerge = array_merge($nn2,$nn);
        $uniqueNN = array_unique($nnMerge);

        $waktuMerge = array_merge($waktu2,$waktu);
        $uniqueWaktu = array_unique($waktuMerge);

        $jarakMerge = array_merge($jarak2,$jarak);
        $uniqueJarak = array_unique($jarakMerge);

        $latlongMerge = array_merge($longlatArr2,$longlatArr);
        $uniqueLatlong = array_unique($latlongMerge);
        $waypoints = array_unique($latlongMerge);
        $finalWaypoints = '';


        $desti = array_values(array_slice($uniqueLatlong, -1))[0];
        $slice = array_pop($waypoints);

        $lastElement = end($waypoints);
        foreach ($waypoints as $key) {
            if ($key != $lastElement) {
            $finalWaypoints .= $key.'|';
            }else{
            $finalWaypoints .= $key;
            }
        }
        // var_dump($finalWaypoints);die;
        $totalWaktu = 0;
        $totalJarak = 0;
        $finalResp = [];

        foreach ($waktuMerge as $waktuFinal) {
            $totalWaktu += $waktuFinal;
        }

        foreach ($jarakMerge as $jarakFinal) {
            $totalJarak += $jarakFinal;
        }

        // print_r($totalWaktu2);die;

        $totalJarakAkhir = $totalJarak/1000;
        $totalWaktuAkhir = $totalWaktu/60;

        foreach ($uniqueNN as $nnFinal) {
            foreach ($data4 as $dataFinal) {
                if ($dataFinal->id_pengiriman == $nnFinal) {
                    array_push($finalResp, $dataFinal);
                }
            }
        }

        $ch2 = curl_init($this->API_Gateway.'/mitra/getKendaraan/'.$this->session->userdata('id_mitra'));
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $server_output3 = curl_exec($ch2);
        $kendaraan = json_decode($server_output3);
        curl_close ($ch2);

        $ch3 = curl_init($this->API_Gateway.'/mitra/getBbmByIdBbm/'.$kendaraan[0]->id_bbm);
        curl_setopt($ch3, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
        $server_output4 = curl_exec($ch3);
        $bbm = json_decode($server_output4);

        curl_close ($ch3);

        if ($kendaraan[0]->jenis_kendaraan == 'mobil') {
            $liter = $totalJarakAkhir/14.8;
            $totalBiayaAkhir = $liter*$bbm->harga;
        }else{
            $liter = $totalJarakAkhir/5.7;
            $totalBiayaAkhir = $liter*$bbm->harga;
        }


        $this->load->view('KomponenMitra/Header');
        $this->load->view('KomponenMitra/Sidebar');
        $this->load->view('Mitra/Pengiriman', compact('finalResp','totalWaktuAkhir','totalJarakAkhir','totalBiayaAkhir','finalWaypoints','desti'));
        $this->load->view('KomponenMitra/Footer');
    }

    function inputStatusPengiriman(){

        $longlat = $this->input->post('longlat');

        $ch = curl_init($this->API_Gateway.'/mitra/getPengirimanPickUpByIdKelompokPengiriman/'.$this->session->userdata('id_mitra'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data = json_decode($server_output);
        // var_dump($data[0]->id_pengiriman);die;

        foreach ($data as $key) {
            // var_dump($key->id_pengiriman);die;
        $status = array(
            'status' => $this->input->post('status'),
            'id' => $key->id_pengiriman,
            );
        // print_r($status);die;

        $ch = curl_init('http://localhost:9000/mitra/inputStatusPengiriman');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($status));
        $server_output = curl_exec($ch);
        curl_close ($ch);
        }

        redirect('mitra/DaftarPengiriman/'.$this->session->userdata('id_mitra').'/'.$longlat);
    }

    function inputStatusPengirimanSampai(){
        $status = array(
            'status' => $this->input->post('status'),
            'id' => $this->input->post('id'),
        );
        $id = $this->input->post('id');
        $longlat = $this->input->post('longlat');
        // print_r($longlat);die;

        $ch = curl_init('http://localhost:9000/mitra/inputStatusPengiriman');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($status));
        $server_output = curl_exec($ch);
        curl_close ($ch);

        $ch2 = curl_init('http://localhost:9000/mitra/updateTanggalSampai/'.$id);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "PUT");
        $server_output2 = curl_exec($ch2);
        $tanggal = json_decode($server_output2);
        curl_close ($ch2);

        $perkiraan = $tanggal->perkiraan_tanggal_sampai;
        $perkiraan = new DateTime($perkiraan); 
        $sekarang = $tanggal->tanggal_sampai;
        $sekarang = new DateTime($sekarang);

        $perbedaan = $perkiraan->diff($sekarang);
        $tepat = 0;
        $terlambat = $perbedaan->d;
        // print_r($terlambat);die;

        if ($tanggal->perkiraan_tanggal_sampai > $tanggal->tanggal_sampai) {
            $ch3 = curl_init('http://localhost:9000/mitra/updateJumlahHariTelat/'.$id.'/'.$tepat);
            curl_setopt($ch3, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, "PUT");
            $server_output3 = curl_exec($ch3);
            curl_close ($ch3);
        }
        else if ($tanggal->perkiraan_tanggal_sampai == $tanggal->tanggal_sampai) {
            $ch3 = curl_init('http://localhost:9000/mitra/updateJumlahHariTelat/'.$id.'/'.$tepat);
            curl_setopt($ch3, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, "PUT");
            $server_output3 = curl_exec($ch3);
            curl_close ($ch3);
        }
        else{
            $ch3 = curl_init('http://localhost:9000/mitra/updateJumlahHariTelat/'.$id.'/'.$terlambat);
            curl_setopt($ch3, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, "PUT");
            $server_output3 = curl_exec($ch3);
            curl_close ($ch3);
        }

        redirect('mitra/DaftarPengiriman/'.$this->session->userdata('id_mitra').'/'.$longlat);

    }

    function inputReportMitra(){
        $id_mitra = $this->session->userdata('id_mitra');
        $longlat = $this->input->post('longlat');

        $ch = curl_init($this->API_Gateway.'/mitra/getPengirimanPickUpByIdKelompokPengiriman/'.$id_mitra);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $pengiriman = json_decode($server_output);

        foreach ($pengiriman as $key) {
            if ($key->id_perusahaan == null) {
                # code...
            }else{
                $data = array(
                    'id_perusahaan' => $key->id_perusahaan,
                    'id_mitra' => $id_mitra,
                    'id_pengiriman' => $key->id_pengiriman,
                    'jenis_masalah' => $this->input->post('jenis_masalah'),
                    'keterangan' => $this->input->post('keterangan'),
                );

                $ch2 = curl_init('http://localhost:9000/mitra/inputReportMitra');
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch2, CURLOPT_POSTFIELDS,http_build_query($data));
                $server_output2 = curl_exec($ch2);
                curl_close ($ch2);
            }

        }

        redirect('mitra/DaftarPengiriman/'.$this->session->userdata('id_mitra').'/'.$longlat);

    }

    function riwayatPengiriman(){
        $ch2 = curl_init($this->API_Gateway.'/mitra/getPendapatanMitra/'.$this->session->userdata('id_mitra'));
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $server_output2 = curl_exec($ch2);
        $data['riwayat2'] = json_decode($server_output2);
        curl_close ($ch2);

        $data['sum'] = 0;
        foreach ($data['riwayat2'] as $key) {
            $data['sum'] += $key->tarif;
        }

        $this->load->view('KomponenMitra/Header');
        $this->load->view('KomponenMitra/Sidebar');
        $this->load->view('Mitra/RiwayatPengiriman', $data);
        $this->load->view('KomponenMitra/Footer');
    }


    function detailDaftarPengiriman($id){
        $ch = curl_init($this->API_Gateway.'/mitra/getPengirimanByIdKelompokPengiriman/'.$id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data['daftar_pengiriman'] = json_decode($server_output);
        curl_close ($ch);
        $data['id_kelompok_pengiriman'] = $data['daftar_pengiriman'][0]->id_kelompok_pengiriman;
        // print_r($data['id_kelompok_pengiriman']); die;
        $this->load->view('KomponenMitra/Header');
        $this->load->view('KomponenMitra/Sidebar');
        $this->load->view('Mitra/DetailDaftarPengiriman',$data);
        $this->load->view('KomponenMitra/Footer');
    }

    function updateStatusPickUp($id){
        $id_mitra = $this->session->userdata('id_mitra');

        $ch4 = curl_init($this->API_Gateway.'/mitra/updateStatusMitra/'.$id_mitra);
        curl_setopt($ch4, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch4, CURLOPT_CUSTOMREQUEST, "PUT");
        $server_output4 = curl_exec($ch4);
        curl_close ($ch4);

        $ch3 = curl_init($this->API_Gateway.'/mitra/updateStatusPickUp/'.$id);
        curl_setopt($ch3, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, "PUT");
        $server_output3 = curl_exec($ch3);
        $pickUp = json_decode($server_output3);
        // var_dump($pickUp);die;
        curl_close ($ch3);


        redirect('mitra/PickUp');
    }
    
    function belumVerifikasi(){
        $this->load->view('Mitra/BelumVerifikasi');
    }

    function daftarMitra(){

        $ch2 = curl_init($this->API_Gateway.'/mitra/getTarifByIdMitra/'.$this->session->userdata('id_mitra'));
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $server_output2 = curl_exec($ch2);
        $data['daftar_pengajuan'] = json_decode($server_output2);
        curl_close ($ch2);

        $this->load->view('KomponenMitra/Header');
        $this->load->view('KomponenMitra/Sidebar');
        $this->load->view('Mitra/DaftarMitra', $data);
        $this->load->view('KomponenMitra/Footer');
    }

    function inputPengiriman(){

        $alamat = $this->input->post('alamat_tujuan');
        $kecamatan = $this->input->post('kecamatan_tujuan');
        $kelurahan = $this->input->post('kelurahan_tujuan');
        $kode = $this->input->post('kode_pos_tujuan');
        $kota = $this->input->post('kota_tujuan');

        $alamatFinal = $alamat.','.$kecamatan.','.$kelurahan.','.$kode.','.$kota.'.';
        $alamatFinal = str_replace(' ', '%20', $alamatFinal);

        $hitGmaps2 = curl_init('https://maps.googleapis.com/maps/api/geocode/json?address='.$alamatFinal.'&key=AIzaSyCgP-WOSIf3GPWcFmPB1IXVMIZPiebhOno');
        curl_setopt($hitGmaps2, CURLOPT_HTTPHEADER, array('Accept:application/json'));
        curl_setopt($hitGmaps2, CURLOPT_RETURNTRANSFER, 1);
        $server_output2 = curl_exec($hitGmaps2);
        $tahap1 = json_decode($server_output2);

        $latitude = $tahap1->results[0]->geometry->location->lat;
        $longitude = $tahap1->results[0]->geometry->location->lng;

        $dataPengiriman = array(
            'no_resi' => $this->input->post('no_resi'), 
            'nama_barang' => $this->input->post('nama_barang'), 
            'jenis_pengiriman' => $this->input->post('jenis_pengiriman'), 
            'jenis_pengangkutan' => $this->input->post('jenis_pengangkutan'), 
            'jenis_barang' => $this->input->post('jenis_barang'), 
            'berat' => $this->input->post('berat'), 
            'dimensi' => $this->input->post('dimensi'), 
            'total_unit' => $this->input->post('total_unit'), 
            'kota_asal' => $this->input->post('kota_asal'), 
            'kota_tujuan' => $this->input->post('kota_tujuan'), 
            'alamat_tujuan' => $this->input->post('alamat_tujuan'), 
            'kecamatan_tujuan' => $this->input->post('kecamatan_tujuan'), 
            'kelurahan_tujuan' => $this->input->post('kelurahan_tujuan'), 
            'kode_pos_tujuan' => $this->input->post('kode_pos_tujuan'), 
            'provinsi_tujuan' => $this->input->post('provinsi_tujuan'),
            'nama_pengirim' => $this->input->post('nama_pengirim'), 
            'nama_penerima' => $this->input->post('nama_penerima'), 
            'no_telp_pengirim' => $this->input->post('no_telp_pengirim'), 
            'no_telp_penerima' => $this->input->post('no_telp_penerima'), 
            'id_kelompok_pengiriman' => $this->input->post('id_kelompok_pengiriman'), 
            'latitude' => $latitude, 
            'longitude' => $longitude, 
        );
        // print_r($dataPengiriman);die;    

        $ch = curl_init('http://localhost:9000/mitra/inputPengiriman');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPengiriman);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($dataPengiriman));
        $server_output = curl_exec($ch);
        $data = json_decode($server_output);
        curl_close ($ch);

        redirect('mitra/detailDaftarPengiriman/'.$this->input->post('id_kelompok_pengiriman'));

    }

    function pengajuanBermitra(){
        if ($this->input->post('minimal_berat') == '') {
            $minimal_berat = 0;
        }else{
            $minimal_berat = $this->input->post('minimal_berat');
        }

        if ($this->input->post('maksimal_berat') == '') {
            $maksimal_berat = 0;
        }else{
            $maksimal_berat = $this->input->post('maksimal_berat');
        }

        if ($this->input->post('minimal_jumlah') == '') {
            $minimal_jumlah = 0;
        }else{
            $minimal_jumlah = $this->input->post('minimal_jumlah');
        }

        if ($this->input->post('maksimal_jumlah') == '') {
            $maksimal_jumlah = 0;
        }else{
            $maksimal_jumlah = $this->input->post('maksimal_jumlah');
        }

        if ($this->input->post('minimal_dimensi') == '') {
            $minimal_dimensi = 0;
        }else{
            $minimal_dimensi = $this->input->post('minimal_dimensi');
        }

        if ($this->input->post('maksimal_dimensi') == '') {
            $maksimal_dimensi = 0;
        }else{
            $maksimal_dimensi = $this->input->post('maksimal_dimensi');
        }

        $dataPengajuan = array(
            'id_perusahaan' => $this->input->post('id_perusahaan'), 
            'id_mitra' => $this->session->userdata('id_mitra'), 
            'asal' => $this->input->post('asal'), 
            'tujuan' => $this->input->post('tujuan'), 
            'jenis_pengiriman' => $this->input->post('jenis_pengiriman'), 
            'moda' => $this->input->post('moda'), 
            'jenis_pengangkutan' => $this->input->post('jenis_pengangkutan'), 
            'nama_barang' => $this->input->post('nama_barang'), 
            'tarif_normal' => $this->input->post('tarif_normal'), 
            'durasi' => $this->input->post('durasi'), 
            'minimal_berat' => $minimal_berat,
            'maksimal_berat' => $maksimal_berat, 
            'minimal_jumlah' => $minimal_jumlah, 
            'maksimal_jumlah' => $maksimal_jumlah, 
            'minimal_dimensi' => $minimal_dimensi, 
            'maksimal_dimensi' => $maksimal_dimensi
        );
        
            $ch = curl_init('http://localhost:9000/mitra/pengajuanTarif');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPengajuan);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($dataPengajuan));
            $server_output = curl_exec($ch);
            $data = json_decode($server_output);
            curl_close ($ch);

        if ($server_output) {
            echo "<script>alert('Berhasil melakukan Pendaftaran!');history.go(-1);</script>";
        }else{
            echo "<script>alert('Gagal melakukan Pendaftaran!');history.go(-1);</script>";
        }
    }

    function updatePengajuanTarif(){
        if ($this->input->post('minimal_berat') == '') {
            $minimal_berat = 0;
        }else{
            $minimal_berat = $this->input->post('minimal_berat');
        }

        if ($this->input->post('maksimal_berat') == '') {
            $maksimal_berat = 0;
        }else{
            $maksimal_berat = $this->input->post('maksimal_berat');
        }

        if ($this->input->post('minimal_jumlah') == '') {
            $minimal_jumlah = 0;
        }else{
            $minimal_jumlah = $this->input->post('minimal_jumlah');
        }

        if ($this->input->post('maksimal_jumlah') == '') {
            $maksimal_jumlah = 0;
        }else{
            $maksimal_jumlah = $this->input->post('maksimal_jumlah');
        }

        if ($this->input->post('minimal_dimensi') == '') {
            $minimal_dimensi = 0;
        }else{
            $minimal_dimensi = $this->input->post('minimal_dimensi');
        }

        if ($this->input->post('maksimal_dimensi') == '') {
            $maksimal_dimensi = 0;
        }else{
            $maksimal_dimensi = $this->input->post('maksimal_dimensi');
        }

        $updatePengajuan = array(
            'id' => $this->input->post('id'), 
            'id_perusahaan' => $this->input->post('id_perusahaan'), 
            'id_mitra' => $this->session->userdata('id_mitra'), 
            'asal' => $this->input->post('asal'), 
            'tujuan' => $this->input->post('tujuan'), 
            'jenis_pengiriman' => $this->input->post('jenis_pengiriman'), 
            'moda' => $this->input->post('moda'), 
            'jenis_pengangkutan' => $this->input->post('jenis_pengangkutan'), 
            'nama_barang' => $this->input->post('nama_barang'), 
            'tarif_normal' => $this->input->post('tarif_normal'), 
            'durasi' => $this->input->post('durasi'), 
            'minimal_berat' => $minimal_berat,
            'maksimal_berat' => $maksimal_berat, 
            'minimal_jumlah' => $minimal_jumlah, 
            'maksimal_jumlah' => $maksimal_jumlah, 
            'minimal_dimensi' => $minimal_dimensi, 
            'maksimal_dimensi' => $maksimal_dimensi
        );
        
            $ch = curl_init('http://localhost:9000/mitra/updatePengajuanTarif/'.$this->input->post('id'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($updatePengajuan));
            $server_output = curl_exec($ch);
            $data = json_decode($server_output);
            curl_close ($ch);

        if ($server_output) {
            echo "<script>alert('Berhasil melakukan Pendaftaran!');history.go(-1);</script>";
        }else{
            echo "<script>alert('Gagal melakukan Pendaftaran!');history.go(-1);</script>";
        }
    }

    function deletePengajuanTarif($id){

            $ch = curl_init('http://localhost:9000/mitra/deletePengajuanTarif/'.$id);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            $server_output = curl_exec($ch);
            curl_close ($ch);

        if ($server_output) {
            echo "<script>alert('Berhasil melakukan Pendaftaran!');history.go(-1);</script>";
        }else{
            echo "<script>alert('Gagal melakukan Pendaftaran!');history.go(-1);</script>";
        }
    }

    function profil(){
        $ch = curl_init($this->API_Gateway.'/showProfil/'.$this->session->userdata('id_users'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data['profil'] = json_decode($server_output);
        curl_close ($ch);

        $this->load->view('KomponenMitra/Header');
        $this->load->view('KomponenMitra/Sidebar');
        $this->load->view('Mitra/profil',$data);
        $this->load->view('KomponenMitra/Footer');
    }

    function updateProfil(){
        $config['upload_path'] = './assets/FotoPendaftaran';
         $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto_diri')){
            $result1 = $this->upload->data();
            $foto_diri = $result1['file_name'];
         }else{
            $foto_diri = '-';
         }
         if ($this->upload->do_upload('foto_ktp')){
            $result2 = $this->upload->data();
            $foto_ktp = $result2['file_name'];
         }else{
            $foto_ktp = '-';
         }
         if ($this->upload->do_upload('foto_npwp')){
            $result3 = $this->upload->data();
            $foto_npwp = $result3['file_name'];
         }else{
            $foto_npwp = '-';
         }

        $dataUpdate = array(
            'email' => $this->input->post('email'), 
            'nama_lengkap' => $this->input->post('nama_lengkap'), 
            'tanggal_lahir' => $this->input->post('tanggal_lahir'), 
            'tempat_lahir' => $this->input->post('tempat_lahir'), 
            'alamat' => $this->input->post('alamat'), 
            'no_telp' => $this->input->post('no_telp'), 
            'nik' => $this->input->post('nik'), 
            'foto_diri' => $foto_diri, 
            'foto_ktp' => $foto_ktp, 
            'foto_npwp' => $foto_npwp
        );

        // var_dump($dataUpdate);die;

            $ch = curl_init($this->API_Gateway.'/updateProfilMitra/'.$this->session->userdata('id_users'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($dataUpdate));
            $server_output = curl_exec($ch);
            $profil = json_decode($server_output);
            // var_dump($profil);die;
            curl_close ($ch);

        redirect('mitra/profil');
    }

    function updateKendaraan(){
        $config['upload_path'] = './assets/FotoPendaftaran';
         $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto_sim')){
            $result1 = $this->upload->data();
            $foto_sim = $result1['file_name'];
         }else{
            $foto_sim = '-';
         }
         if ($this->upload->do_upload('foto_stnk')){
            $result2 = $this->upload->data();
            $foto_stnk = $result2['file_name'];
         }else{
            $foto_stnk = '-';
         }

        $dataUpdate = array(
            'jenis_kendaraan' => $this->input->post('jenis_kendaraan'), 
            'nama_kendaraan' => $this->input->post('nama_kendaraan'), 
            'plat_kendaraan' => $this->input->post('plat_kendaraan'), 
            'tahun_kendaraan' => $this->input->post('tahun_kendaraan'), 
            'domisili' => $this->input->post('domisili'), 
            'status_ketersediaan' => $this->input->post('status_ketersediaan'),
            'id_bbm' => $this->input->post('id_bbm'),
            'foto_sim' => $foto_sim, 
            'foto_stnk' => $foto_stnk
        );

        // print_r($dataUpdate);die;

            $ch = curl_init($this->API_Gateway.'/mitra/updateKendaraanMitra/'.$this->session->userdata('id_users'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($dataUpdate));
            $server_output = curl_exec($ch);
            $profil = json_decode($server_output);
            // var_dump($profil);die;  
            curl_close ($ch);

        redirect('mitra/kendaraan');
    }

    function kendaraan(){
        $ch = curl_init($this->API_Gateway.'/mitra/getKendaraan/'.$this->session->userdata('id_mitra'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->session->userdata('token')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data['kendaraan'] = json_decode($server_output);
        curl_close ($ch);
        // print_r($server_output);die;

        $this->load->view('KomponenMitra/Header');
        $this->load->view('KomponenMitra/Sidebar');
        $this->load->view('Mitra/kendaraan',$data);
        $this->load->view('KomponenMitra/Footer');
    }

    public function Logout()
    {
        $this->session->sess_destroy();
        redirect(base_url("index.php/Home"));
    }

}