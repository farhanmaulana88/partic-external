<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Sahabat Partic Transportasi  </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/landingpage/assets/img/logo/partic-logo.ico">

    <!-- CSS here -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/slicknav.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/nice-select.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landingpage/assets/css/style.css">
</head>
<body>
<!--? Preloader Start -->
<div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="<?php echo base_url();?>assets/landingpage/assets/img/logo/partic-logo.png" alt="">
            </div>
        </div>
    </div>
</div>
<!-- Preloader Start -->
<header>
    <!-- Header Start -->
    <div class="header-area">
        <div class="main-header ">
            <div class="header-top d-none d-lg-block">
                <div class="container">
                    <div class="col-xl-12">
                        <div class="row d-flex justify-content-between align-items-center">
                            <div class="header-info-left">
                            </div>
                            <div class="header-info-right">
                                <ul class="header-social">    
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li> <a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom  header-sticky">
                <div class="container">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-2 col-lg-2">
                            <div class="logo">
                                <a href="index.html"><img style="width: 300px; height: 100px" src="<?php echo base_url();?>assets/landingpage/assets/img/logo/partic-1.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-10">
                            <div class="menu-wrapper  d-flex align-items-center justify-content-end">
                                <!-- Main-menu -->
                                <div class="main-menu d-none d-lg-block">
                                    <nav> 
                                        <ul id="navigation">                                                                                          
                                            <li><a href="<?php echo base_url();?>index.php/home">Home</a></li>
                                            <li><a href="<?php echo base_url();?>index.php/home/cekResi">Cek Resi</a></li>
                                            <li><a>Pendaftaran</a>
                                                <ul class="submenu">
                                                    <li><a href="<?php echo base_url(). 'index.php/home/formPendaftaranKurir'; ?>">Kurir</a></li>
                                                    <li><a href="<?php echo base_url(). 'index.php/home/formPendaftaranPelanggan'; ?>">Member</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <!-- Header-btn -->
                                <div class="header-right-btn d-none d-lg-block ml-20">
                                        <!-- Trigger the modal with a button -->
                                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Login</button>
                                </div>
                            </div>
                        </div> 
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
    <!-- Modal -->
                                    <div id="myModal" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h4 class="modal-title">Login</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          </div>
                                          <div class="modal-body">
                                            <div class="contact-form-wrapper">
                                            <form action="<?php echo base_url(). 'index.php/home/login'; ?>" class="contact-form" method="POST">
                                                <div class="row ">
                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="input-form">
                                                            <input type="email" placeholder="Email" name="email" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6">
                                                        <div class="input-form">
                                                            <input type="password" placeholder="Password" name="password" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="submit" class="btn btn-default">Login</button>
                                            <button class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                            </form>
                                        </div>

                                      </div>
                                    </div>
</header>