        <div class="navbar-default sidebar nicescroll" role="navigation">
            <div class="sidebar-nav navbar-collapse ">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="ti-search"></i> </button>
                            </span>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/member/daftarMember" class="waves-effect"><i class="ti-info fa-fw"></i> Daftar Member</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/member/profil" class="waves-effect"><i class="ti-user fa-fw"></i> Profil</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/member/DaftarPengiriman/'.$this->session->userdata('id_users'));?>" class="waves-effect"><i class="ti-package fa-fw"></i> Pengiriman</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/member/riwayatPengiriman');?>" class="waves-effect"><i class="ti-clipboard fa-fw"></i> Riwayat Pengiriman</a>
                    </li>
                </ul>
                <div class="center p-20">
                    <span class="hide-menu"><a href="<?php echo base_url();?>index.php/member/logout" class="btn btn-info btn-block btn-rounded waves-effect waves-light">Logout</a></span>
                </div>
            </div>
            <!-- /.sidebar-collapse -->
        </div>