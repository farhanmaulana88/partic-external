        <div class="navbar-default sidebar nicescroll" role="navigation">
            <div class="sidebar-nav navbar-collapse ">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="ti-search"></i> </button>
                            </span>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/mitra/daftarMitra" class="waves-effect"><i class="ti-info fa-fw"></i> Daftar Mitra</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/mitra/profil" class="waves-effect"><i class="ti-user fa-fw"></i> Profil</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/mitra/kendaraan" class="waves-effect"><i class="ti-truck fa-fw"></i> Kendaraan</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/mitra/pickUp" class="waves-effect"><i class="ti-layout fa-fw"></i> Pick Up</a>
                    </li>
                    <li>
                        <a href="#" id="longlat" class="waves-effect"><i class="ti-package fa-fw"></i> Pengiriman Asc</a>
                        <input id="id_mitra" type="text" hidden value="<?php echo $this->session->userdata('id_mitra');?>">
                    </li>
                    <li>
                        <a href="#" id="longlat2" class="waves-effect"><i class="ti-package fa-fw"></i> Pengiriman Desc</a>
                        <input id="id_mitra" type="text" hidden value="<?php echo $this->session->userdata('id_mitra');?>">
                    </li>
                    <li>
                        <a href="<?php echo base_url('index.php/mitra/riwayatPengiriman');?>" class="waves-effect"><i class="ti-clipboard fa-fw"></i> Riwayat Pengiriman</a>
                    </li>
                </ul>
                <div class="center p-20">
                    <span class="hide-menu"><a href="<?php echo base_url();?>index.php/mitra/logout" class="btn btn-info btn-block btn-rounded waves-effect waves-light">Logout</a></span>
                </div>
            </div>
            <!-- /.sidebar-collapse -->
        </div>

        <script type="text/javascript">
            $(document).ready(function (){
            // var x = document.getElementById("demo");
              if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
              } else {
                alert("Geolocation is not supported by this browser.")
              }

            function showPosition(position) {
            window.longlat = position.coords.latitude + ',' + position.coords.longitude
            }
            });

            $("#longlat").click(function(){
                // alert(window.longlat)
                var base_url = "<?php echo base_url();?>"
                var session = $("#id_mitra").val()
                console.log(session);
                window.location.assign(base_url + 'index.php/mitra/DaftarPengiriman/' + session + '/' + '-6.8943609,107.67668130000001');
              });

            $("#longlat2").click(function(){
                // alert('-6.8943609,107.67668130000001')
                var base_url = "<?php echo base_url();?>"
                var session = $("#id_mitra").val()
                console.log(session);
                window.location.assign(base_url + 'index.php/mitra/DaftarPengirimanDesc/' + session + '/' + '-6.8943609,107.67668130000001');
              });
        </script>