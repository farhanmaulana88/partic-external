
<main>
    <!--? slider Area Start-->
    <div class="slider-area ">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-9 col-lg-9">
                            <div class="hero__caption">
                                <h1 >Safe & Reliable <span>Logistic</span> Solutions!</h1>
                            </div>
                            <!--Hero form -->
                            <form action="<?php echo base_url();?>index.php/home/hasilResi" class="search-box" method="post">
                                <div class="input-form">
                                    <input type="text" placeholder="Your Tracking ID" name="no_resi">
                                </div>
                                <div class="search-form">
                                    <button type="submit" class="submit-btn" style="height: 60px">Track & Trace</button>
                                </div>	
                            </form>	
                            <!-- Hero Pera -->
                            <div class="hero-pera">
                                <p>For order status inquiry</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider Area End-->
    <!--? Categories Area Start -->
    <div class="categories-area section-padding30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Section Tittle -->
                    <div class="section-tittle text-center mb-40">
                        <img style="width: 150px; height: 170px" src="<?php echo base_url();?>assets/landingpage/assets/img/logo/partic-logo.png" alt="">
                        <span>Service Kami</span>
                        <h2>Lacak Kiriman Partic</h2>
                    </div>
                </div>
            </div>
            <center>
            <table border="1px" style="width:100%">
                    <tr>
                        <td colspan="2" style="font-size: 30px; background-color: #e535ab;"><center><b>Data Kiriman</b></center></td>
                    </tr>
                    <?php 
                    foreach ($keterangan as $foo) { 
                        $tujuan = $foo->alamat_tujuan.', '.$foo->kecamatan_tujuan.', '.$foo->kelurahan_tujuan.', '.$foo->kode_pos_tujuan.', '.$foo->kota_tujuan.'.';
                        ?>
                  <tr>
                    <th style="width: 300px; height: 50px; font-size: 20px">Nomer Resi:</th>
                    <td style="font-size: 20px"><?php echo $foo->no_resi;?></td>
                  </tr>
                  <tr>
                    <th style="width: 300px; height: 50px; font-size: 20px">Jenis Pengiriman:</th>
                    <td style="font-size: 20px"><?php echo $foo->jenis_pengiriman;?></td>
                  </tr>
                  <tr>
                    <th style="width: 300px; height: 50px; font-size: 20px">Tanggal Pengiriman:</th>
                    <td style="font-size: 20px"><?php echo $foo->tanggal_pengiriman;?></td>
                  </tr>
                  <tr>
                    <th style="width: 300px; height: 50px; font-size: 20px">Perkiraan Sampai:</th>
                    <td style="font-size: 20px"><?php echo $foo->perkiraan_tanggal_sampai;?></td>
                  </tr>
                  <tr>
                    <th style="width: 300px; height: 50px; font-size: 20px">Nama Pengirim:</th>
                    <td style="font-size: 20px"><?php echo $foo->nama_pengirim;?></td>
                  </tr>
                  <tr>
                    <th style="width: 300px; height: 50px; font-size: 20px">Nama Penerima:</th>
                    <td style="font-size: 20px"><?php echo $foo->nama_penerima;?></td>
                  </tr>
                  <tr>
                    <th style="width: 300px; height: 50px; font-size: 20px">Alamat Tujuan:</th>
                    <td style="font-size: 20px"><?php echo $tujuan;?></td>
                  </tr>
              <?php } ?>
                </table>
                    <!-- ========================================= -->
                <table border="1px" style="width:100%">
                    <tr>
                        <td colspan="3" style="font-size: 30px; background-color: #e535ab"><center><b>Status Detail</b></center></td>
                    </tr>
                    <tr>
                        <th style="width: 30px; height: 50px; font-size: 20px"><center>No.</center></th>
                        <th style="width: 300px; height: 50px; font-size: 20px"><center>Tanggal</center></th>
                        <th style="width: 300px; height: 50px; font-size: 20px"><center>Status</center></th>
                    </tr>
                    <?php 
                        $no = 0;
                        foreach ($status as $key) { 
                        $no++;
                    ?>
                    <tr>
                        <td style="height: 50px;font-size: 20px"><center><?php echo $no;?></center></td>
                        <td style="height: 50px;font-size: 20px"><?php echo $key->tanggal;?></td>
                        <td style="height: 50px;font-size: 20px"><?php echo $key->status;?></td>
                    </tr>
                    <?php 
                        }
                    ?>
                </table>
            </center>
        </div>
    </div>
    <!-- Categories Area End -->
</main>