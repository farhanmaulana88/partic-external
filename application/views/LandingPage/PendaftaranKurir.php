 <main>
        <!--? slider Area Start-->
        <div class="slider-area ">
            <div class="single-slider hero-overly slider-height2 d-flex align-items-center" data-background="<?php echo base_url();?>assets/landingpage/assets/img/hero/about.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap">
                                <h2>Pendaftaran Kurir</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Beranda</a></li>
                                        <li class="breadcrumb-item"><a href="#">Pendaftaran Kurir</a></li> 
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        <!--? our info Start -->
    <div class="our-info-area pt-70 pb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="flaticon-support"></span>
                        </div>
                        <div class="info-caption">
                            <p>Call Us Anytime</p>
                            <span>+ (123) 1800-567-8990</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="flaticon-clock"></span>
                        </div>
                        <div class="info-caption">
                            <p>Sunday CLOSED</p>
                            <span>Mon - Sat 8.00 - 18.00</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="flaticon-place"></span>
                        </div>
                        <div class="info-caption">
                            <p>Columbia, SC 29201</p>
                            <span>USA, New York - 10620</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- our info End -->
        <!--? contact-form start -->
        <section class="contact-form-area section-bg  pt-115 pb-120 fix" data-background="<?php echo base_url();?>assets/landingpage/assets/img/gallery/section_bg02.jpg">
            <div class="container">
                <div class="row justify-content-end">
                    <!-- Contact wrapper -->
                    <div class="col-xl-8 col-lg-9">
                        <div class="contact-form-wrapper">
                            <!-- From tittle -->
                            <div class="row">
                               <div class="col-lg-12">
                                    <!-- Section Tittle -->
                                    <div class="section-tittle mb-20">
                                        <span>Get a Qote For Free</span>
                                        <h2>Biodata Kurir</h2>
                                    </div>
                               </div>
                            </div>
                            <!-- form -->
                            <form action="<?php echo base_url(). 'index.php/home/pendaftaranKurir'; ?>" class="contact-form" method="POST" enctype="multipart/form-data">
                                <div class="row ">
                                    <div class="col-lg-12">
                                        <div class="input-form">
                                            <input type="text" placeholder="Nama Lengkap" name="nama_lengkap" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-form">
                                            <input type="email" placeholder="Email" name="email" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-form">
                                            <input type="password" placeholder="Password" name="password" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-form">
                                            <input type="text" placeholder="Tempat Lahir" name="tempat_lahir" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-form">
                                            <input type="date" placeholder="Tanggal Lahir" name="tanggal_lahir" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="input-form">
                                            <input type="text" placeholder="Alamat" name="alamat" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="input-form">
                                            <input type="number" placeholder="Nomer NIK" name="nik" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="input-form">
                                            <input type="number" placeholder="Nomer Telpon" name="no_telp" required>
                                        </div>
                                    </div>
                                    <!-- Height Width length -->
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <div class="input-form">
                                            Foto KTP
                                            <input type="file" name="foto_ktp" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                        <div class="input-form">
                                            Foto NPWP
                                            <input type="file" name="foto_npwp" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6" required>
                                        <div class="input-form">
                                            Foto Diri
                                            <input type="file" name="foto_diri">
                                        </div>
                                    </div>

            <!-- Data Kendaraan -->
                            <br><br> <br><br><br>

                            <div class="row">
                               <div class="col-lg-12">
                                    <!-- Section Tittle -->
                                    <div class="section-tittle mb-20">
                                        <h2>Keterangan Kendaraan Kurir</h2>
                                    </div>
                               </div>
                            </div>

                            <div class="row ">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-form">
                                            <input type="text" placeholder="Nama Kendaraan" name="nama_kendaraan" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-form">
                                            <input type="text" placeholder="Plat Kendaraan" name="plat_kendaraan" required>
                                        </div>
                                    </div>
                                    <!-- Radio Button -->
                                <div class="col-lg-12">
                                    <div class="radio-wrapper mb-30 mt-15">
                                        <label>Jenis Kendaraan:</label>
                                        <div class="select-radio">
                                            <div class="radio">
                                                <input id="radio-2" name="jenis_kendaraan" type="radio" value="mobil">
                                                <label for="radio-2" class="radio-label">Mobil</label>
                                            </div>
                                            <div class="radio">
                                                <input id="radio-4" name="jenis_kendaraan" type="radio" value="truck">
                                                <label for="radio-4" class="radio-label">Truck</label>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <!-- Button -->
                                    <div class="col-lg-4 col-md-4">
                                        <div class="input-form">
                                            <input type="number" placeholder="Tahun Kendaraan" name="tahun_kendaraan" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="input-form">
                                            <input type="text" placeholder="Domisili" name="domisili" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="input-form">
                                            <select name="id_bbm">
                                                <?php foreach ($kendaraan as $key) { ?>
                                                    <option value="<?php echo $key->id;?>"><?php echo $key->nama_bbm;?></option>        
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Height Width length -->
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="input-form">
                                            Foto STNK
                                            <input type="file" name="foto_stnk" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="input-form">
                                            Foto SIM
                                            <input type="file" name="foto_sim" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <button type="submit" class="submit-btn">Request a Quote</button>
                                    </div>
                                </div>
                            </form>	
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- contact-form end -->
        <!--? our info Start -->
    <div class="our-info-area pt-70 pb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="flaticon-support"></span>
                        </div>
                        <div class="info-caption">
                            <p>Call Us Anytime</p>
                            <span>+ (123) 1800-567-8990</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="flaticon-clock"></span>
                        </div>
                        <div class="info-caption">
                            <p>Sunday CLOSED</p>
                            <span>Mon - Sat 8.00 - 18.00</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-info mb-30">
                        <div class="info-icon">
                            <span class="flaticon-place"></span>
                        </div>
                        <div class="info-caption">
                            <p>Columbia, SC 29201</p>
                            <span>USA, New York - 10620</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- our info End -->
    </main>