<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <title>My Admin - is a responsive admin template</title>
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>assets/mitramember/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/mitramember/css/style.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shiv and Respond.js IE8 support -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <section id="wrapper" class="error-page">
        <div class="error-box">
            <div class="error-body text-center">
                <h1>Pending</h1>
                <h3 class="text-uppercase">Verifikasi akun anda terlebih dahulu kemudian coba login kembali.</h3>
                <p class="text-muted m-t-30 m-b-30">Kami sudah mengirimkan link ke email anda untuk aktifasi akun.</p>
                <a href="<?php echo base_url();?>index.php/mitra/logout" class="btn btn-primary btn-rounded waves-effect waves-light m-b-40">Logout</a> </div>
            <footer class="footer text-center">2017 © my Admin.</footer>
        </div>
    </section>
</body>

</html>
