        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Daftar Cabang</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Mitra Member</a></li>
                            <li><a href="<?php echo base_url();?>index.php/member/daftarMember">Halaman Daftar Member</a></li>
                            <li class="active">Halaman Detail Cabang</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Daftar Lokasi Cabang</h3>
                            <div class="table-responsive">
                                <table id="myTable" class="table">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Regional</th>
                                            <th>Cabang</th>
                                            <th>Alamat Cabang</th>
                                            <th>No Telp Cabang</th>
                                            <th>Cek Lokasi</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    $no =0;
                                    foreach ($cabang_perusahaan as $key) {
                                    $no++;
                                    ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $key->kota;?>, <?php echo $key->provinsi;?>.</td>
                                            <td><?php echo $key->nama_cabang;?></td>
                                            <td><?php echo $key->alamat_cabang;?></td>
                                            <td><?php echo $key->no_telp_cabang;?></td>
                                            <td></td>
                                        </tr>            
                                    </tbody>
                                <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
