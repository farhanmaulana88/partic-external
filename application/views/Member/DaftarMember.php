    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-12">
                    <h4 class="page-title">Pendaftaran Bermember</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Mitra Member</a></li>
                        <li class="active">Halaman Daftar Member</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
                        <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-block btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#myModal">Ajukan Tawaran Bermember</button>

            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Penawaran Menjadi Member</h4>
                  </div>
                  <div class="modal-body">
                    <div class="col-md-12 col-xs-12">
                    <div class="white-box">
                        <form action="<?php echo base_url();?>index.php/member/pengajuanBermember" method="POST" class="form-horizontal form-material">
                            <div class="form-group">
                                <label class="col-sm-12">Perusahaan</label>
                                <div class="col-sm-12">
                                    <select name="id_perusahaan" class="form-control form-control-line" required>
                                        <?php foreach ($nama_perusahaan as $key) { ?>
                                        <option value="<?php echo $key->id;?>"><?php echo $key->nama_perusahaan;?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <p style="color: red">*Dengan mengajukan tawaran menjadi member, anda akan mendapatkan keuntungan yang diberikan oleh aplikasi.</p>
                            </div>
                    </div>
                </div>
                  </div>
                  <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Ajukan Tawaran</button>
                        </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                  </div>
                </div>

              </div>
            </div>
                <!-- /Modal content-->

                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Daftar Keterangan Penawaran Bermember</h3>
                            <div class="table-responsive">
                                <table id="myTable" class="table">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Perusahaan</th>
                                            <th>No. Telp Perusahaan</th>
                                            <th>Verifikasi</th>
                                            <th style="width: 40px"></th>
                                        </tr>
                                    </thead>
                                    <?php 
                                    $no = 0;
                                    foreach ($daftar_member as $test) { 
                                    $no++;
                                        ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $test->nama_perusahaan_tampilan;?></td>
                                            <td><?php echo $test->no_telp_tampilan;?></td>
                                            <td>
                                                <?php
                                                    if ($test->verifikasi == 2) {
                                                         echo "Belum Mendapat Persetujuan";
                                                     }else{
                                                         echo "Mendapat Persetujuan";
                                                }?>        
                                            </td>
                                            <td>
                                                <?php if ($test->verifikasi == 1) {?>
                                                <a href="<?php echo base_url('index.php/member/getDataCabang/'.$test->id_perusahaan);?>" class="btn btn-info"><i class="ti-info fa-fw"></i></a>
                                                <?php }else{ ?>
                                                <a href="#" disabled class="btn btn-info"><i class="ti-info fa-fw"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <!-- /.container-fluid -->
    </div>