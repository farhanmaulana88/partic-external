        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Sedang Dikirim</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Mitra Member</a></li>
                            <li class="active">Halaman Pengiriman</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Daftar Lokasi Pick Up Barang</h3>
                            <div class="table-responsive">
                                <table id="myTable" class="display" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Perusahaan</th>
                                            <th>Cabang</th>
                                            <th>Alamat Cabang</th>
                                            <th>Berat</th>
                                            <th>Total Unit</th>
                                            <th>Pengirim</th>
                                            <th>Penerima</th>
                                            <th>No Telp Penerima</th>
                                            <th>Alamat Pengiriman</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $no =0;
                                    foreach ($daftar_pengiriman as $key) {
                                    $no++;
                                    $tujuan = $key->alamat_tujuan.', '.$key->kecamatan_tujuan.', '.$key->kelurahan_tujuan.', '.$key->kode_pos_tujuan.', '.$key->kota_tujuan.'.';
                                    ?>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $key->nama_perusahaan;?></td>
                                            <td><?php echo $key->nama_cabang;?></td>
                                            <td><?php echo $key->alamat;?></td>
                                            <?php
                                            if ($key->jenis_pengangkutan == 'kilo') { ?>
                                                <td><?php echo $key->berat;?></td>
                                                <td><?php echo "-";?></td>
                                             <?php }else{ ?>
                                                <td><?php echo "-";?></td>
                                                <td><?php echo $key->total_unit;?></td>
                                             <?php } ?>
                                            <td><?php echo $key->nama_pengirim;?></td>
                                            <td><?php echo $key->nama_penerima;?></td>
                                            <td><?php echo $key->no_telp_penerima;?></td>
                                            <td><?php echo $tujuan;?></td>
                                            <td>
                                                <a href="<?php echo base_url('index.php/member/statusPengiriman/'.$key->no_resi);?>" class="btn btn-info"><i class="ti-info fa-fw"></i></a>
                                            </td>
                                        </tr>
                                        
                                <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
