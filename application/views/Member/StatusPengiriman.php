        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Cek Resi Pengiriman</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Mitra Member</a></li>
                            <li><a href="<?php echo base_url('index.php/member/daftarPengiriman/'.$this->session->userdata('id_users'));?>">Halaman Pengiriman</a></li>
                            <li class="active">Halaman Status Pengiriman</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Status Pengiriman Barang</h3>
                        <!--? Categories Area Start -->
                            <div class="categories-area section-padding30">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <!-- Section Tittle -->
                                            <div class="section-tittle text-center mb-40">
                                                <img style="width: 150px; height: 170px" src="<?php echo base_url();?>assets/landingpage/assets/img/logo/partic-logo.png" alt=""><br>
                                                <span>Service Kami</span>
                                                <h2>Lacak Kiriman Partic</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <center>
                                    <table border="1px" style="width:100%">
                                            <tr>
                                                <td colspan="2" style="font-size: 30px; font-color: blue ;background-color: #e535ab;"><center>Data Kiriman</center></td>
                                            </tr>
                                            <?php 
                                            foreach ($keterangan as $foo) { 
                                                $tujuan = $foo->alamat_tujuan.', '.$foo->kecamatan_tujuan.', '.$foo->kelurahan_tujuan.', '.$foo->kode_pos_tujuan.', '.$foo->kota_tujuan.'.';
                                                ?>
                                          <tr>
                                            <th style="width: 300px; height: 50px; font-size: 20px">Nomer Resi:</th>
                                            <td style="font-size: 20px"><?php echo $foo->no_resi;?></td>
                                          </tr>
                                          <tr>
                                            <th style="width: 300px; height: 50px; font-size: 20px">Jenis Pengiriman:</th>
                                            <td style="font-size: 20px"><?php echo $foo->jenis_pengiriman;?></td>
                                          </tr>
                                          <tr>
                                            <th style="width: 300px; height: 50px; font-size: 20px">Tanggal Pengiriman:</th>
                                            <td style="font-size: 20px"><?php echo $foo->tanggal_pengiriman;?></td>
                                          </tr>
                                          <tr>
                                            <th style="width: 300px; height: 50px; font-size: 20px">Perkiraan Sampai:</th>
                                            <td style="font-size: 20px"><?php echo $foo->perkiraan_tanggal_sampai;?></td>
                                          </tr>
                                          <tr>
                                            <th style="width: 300px; height: 50px; font-size: 20px">Nama Pengirim:</th>
                                            <td style="font-size: 20px"><?php echo $foo->nama_pengirim;?></td>
                                          </tr>
                                          <tr>
                                            <th style="width: 300px; height: 50px; font-size: 20px">Nama Penerima:</th>
                                            <td style="font-size: 20px"><?php echo $foo->nama_penerima;?></td>
                                          </tr>
                                          <tr>
                                            <th style="width: 300px; height: 50px; font-size: 20px">Alamat Tujuan:</th>
                                            <td style="font-size: 20px"><?php echo $tujuan;?></td>
                                          </tr>
                                      <?php } ?>
                                        </table>
                                            <!-- ========================================= -->
                                        <table border="1px" style="width:100%">
                                            <tr>
                                                <td colspan="3" style="font-size: 30px; background-color: #e535ab"><center><b>Status Detail</b></center></td>
                                            </tr>
                                            <tr>
                                                <th style="width: 30px; height: 50px; font-size: 20px"><center>No.</center></th>
                                                <th style="width: 300px; height: 50px; font-size: 20px"><center>Tanggal</center></th>
                                                <th style="width: 300px; height: 50px; font-size: 20px"><center>Status</center></th>
                                            </tr>
                                            <?php 
                                                $no = 0;
                                                foreach ($status as $key) { 
                                                $no++;
                                            ?>
                                            <tr>
                                                <td style="height: 50px;font-size: 20px"><center><?php echo $no;?></center></td>
                                                <td style="height: 50px;font-size: 20px"><?php echo $key->tanggal;?></td>
                                                <td style="height: 50px;font-size: 20px"><?php echo $key->status;?></td>
                                            </tr>
                                            <?php 
                                                }
                                            ?>
                                        </table>
                                    </center>
                                </div>
                            </div>
                            <!-- Categories Area End -->
                        </div>
                    </div>
                </div>