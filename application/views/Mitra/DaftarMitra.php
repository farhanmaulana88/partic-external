    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-12">
                    <h4 class="page-title">Pendaftaran Bermitra</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Mitra Kurir</a></li>
                        <li class="active">Halaman Daftar Mitra</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-block btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#myModal">Ajukan Tawaran Bermitra</button>

                <!-- Modal -->
                <div class="modal fade" id="myModal">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="tf-ion-close"></i>
                  </button>
                    <div class="modal-dialog " role="document">
                      <div class="modal-content">
                         <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title">Pengajuan Penawaran Tarif Kerjasama</h3>
                        </div>
                          <div class="modal-body">
                            <form action="<?php echo base_url(). 'index.php/mitra/pengajuanBermitra'; ?>" method="post">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Asal</span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="asal">
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Tujuan</span>
                                        <input style="font-size: 15px" type="text" class="form-control" name="tujuan">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Jenis Pengiriman</span>
                                        <select name="jenis_pengiriman" class="form-control form-control-line" required>
                                            <option value="reguler">Reguler</option>
                                            <option value="ekspres">Ekspres</option>
                                        </select>
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Jenis Pengangkutan</span>
                                        <select name="jenis_pengangkutan" class="form-control form-control-line" required>
                                            <option value="kilo">kilo</option>
                                            <option value="koli">koli</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Nama Barang <b style="color: red">*</b></span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="nama_barang">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <span>Moda</span>
                                        <select name="moda" class="form-control form-control-line" required>
                                            <option value="Darat">Darat</option>
                                            <option value="Laut">Laut</option>
                                            <option value="Udara">Udara</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <span>Tarif Normal</span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="tarif_normal">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <span>Durasi</span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="durasi">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-12">Min. Berat <b style="color: red">*</b></label>
                                        <input type="text" name="minimal_berat" placeholder="Dalam Kg" class="form-control form-control-line"> 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-12">Max. Berat <b style="color: red">*</b></label>
                                        <input type="text" name="maksimal_berat" placeholder="Dalam Kg" class="form-control form-control-line"> 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-12">Min. Jumlah <b style="color: red">*</b></label>
                                        <input type="text" name="minimal_jumlah" placeholder="Dalam Barang Satuan" class="form-control form-control-line"> 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-12">Max. Jumlah <b style="color: red">*</b></label>
                                        <input type="text" name="maksimal_jumlah" placeholder="Dalam Barang Satuan" class="form-control form-control-line"> 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-12">Min. Dimensi <b style="color: red">*</b></label>
                                        <input type="text" name="minimal_dimensi" placeholder="Dalam PxL" class="form-control form-control-line"> 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-12">Max. Dimensi <b style="color: red">*</b></label>
                                        <input type="text" name="maksimal_dimensi" placeholder="Dalam PxL" class="form-control form-control-line"> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <p style="color: red">*Kolom yang ditandai dengan bintang warna merah adalah boleh tidak di isi, sesuaikan dengan kebutuhan tawaran pengiriman anda.</p>
                            </div>

                              <button type="submit" class="btn btn-success">Submit</button>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </form>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- /Modal content-->


                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Daftar Keterangan Penawaran Bermitra</h3>
                            <div class="table-responsive">
                                <table id="myTable" class="table">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Asal</th>
                                            <th>Tujuan</th>
                                            <th>Tarif</th>
                                            <th>Jenis Pengiriman</th>
                                            <th>Durasi</th>
                                            <th style="width: 40px"></th>
                                        </tr>
                                    </thead>
                                    <?php 
                                    $no = 0;
                                    foreach ($daftar_pengajuan as $test) { 
                                    $no++;
                                        ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $test->asal;?></td>
                                            <td><?php echo $test->tujuan;?></td>
                                            <td><?php echo $test->tarif_normal;?></td>
                                            <td><?php echo $test->jenis_pengiriman;?></td>
                                            <td><?php echo $test->durasi;?></td>
                                            <td>
                                                <a class="btn btn-success" data-toggle='modal' data-target='#editPenawaran<?= $test->id; ?>'><i class="ti-pencil-alt fa-fw"></i></a>
                                                <a href="<?php echo base_url('index.php/mitra/deletePengajuanTarif/'.$test->id);?>" class="btn btn-info"><i class="ti-trash fa-fw"></i></a>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                            <div class="modal fade" id="editPenawaran<?php echo $test->id;?>">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="tf-ion-close"></i>
                              </button>
                                <div class="modal-dialog " role="document">
                                  <div class="modal-content">
                                     <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h3 class="modal-title">Edit Penawaran Tarif Kerjasama</h5>
                                                </div>
                                      <div class="modal-body">

                                            <form action="<?php echo base_url(). 'index.php/mitra/updatePengajuanTarif'; ?>" method="post">

                                            <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="id" value="<?php echo $test->id?>">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Asal</span>
                                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="asal" value="<?php echo $test->asal?>">
                                                    </div>
                                                </div>
                                            
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Tujuan</span>
                                                        <input style="font-size: 15px" type="text" class="form-control" name="tujuan" value="<?php echo $test->tujuan?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Jenis Pengiriman</span>
                                                        <select name="jenis_pengiriman" class="form-control form-control-line" required>
                                                            <option selected value="<?php echo $test->jenis_pengiriman;?>"><?php echo $test->jenis_pengiriman;?></option>
                                                            <option value="reguler">Reguler</option>
                                                            <option value="ekspres">Ekspres</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Jenis Pengangkutan</span>
                                                        <select name="jenis_pengangkutan" class="form-control form-control-line" required>
                                                            <option selected value="<?php echo $test->jenis_pengangkutan;?>"><?php echo $test->jenis_pengangkutan;?></option>
                                                            <option value="kilo">kilo</option>
                                                            <option value="koli">koli</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <span>Nama Barang <b style="color: red">*</b></span>
                                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="nama_barang" value="<?php echo $test->nama_barang?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <span>Moda</span>
                                                        <select name="moda" class="form-control form-control-line" required>
                                                            <option selected value="<?php echo $test->moda;?>"><?php echo $test->moda;?></option>
                                                            <option value="Darat">Darat</option>
                                                            <option value="Laut">Laut</option>
                                                            <option value="Udara">Udara</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <span>Tarif Normal</span>
                                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="tarif_normal" value="<?php echo $test->tarif_normal?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <span>Durasi</span>
                                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="durasi" value="<?php echo $test->durasi?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Min. Berat <b style="color: red">*</b></label>
                                                        <input type="text" value="<?php echo $test->minimal_berat ;?>" name="minimal_berat" placeholder="Dalam Kg" class="form-control form-control-line"> 
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Max. Berat <b style="color: red">*</b></label>
                                                        <input type="text" value="<?php echo $test->maksimal_berat ;?>" name="maksimal_berat" placeholder="Dalam Kg" class="form-control form-control-line"> 
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Min. Jumlah <b style="color: red">*</b></label>
                                                        <input type="text" value="<?php echo $test->minimal_jumlah ;?>" name="minimal_jumlah" placeholder="Dalam Barang Satuan" class="form-control form-control-line"> 
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Max. Jumlah <b style="color: red">*</b></label>
                                                        <input type="text" value="<?php echo $test->maksimal_jumlah ;?>" name="maksimal_jumlah" placeholder="Dalam Barang Satuan" class="form-control form-control-line"> 
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Min. Dimensi <b style="color: red">*</b></label>
                                                        <input type="text" value="<?php echo $test->minimal_dimensi ;?>" name="minimal_dimensi" placeholder="Dalam PxL" class="form-control form-control-line"> 
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Max. Dimensi <b style="color: red">*</b></label>
                                                        <input type="text" value="<?php echo $test->maksimal_dimensi ;?>" name="maksimal_dimensi" placeholder="Dalam PxL" class="form-control form-control-line"> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <p style="color: red">*Kolom yang ditandai dengan bintang warna merah adalah boleh tidak di isi, sesuaikan dengan kebutuhan tawaran pengiriman anda.</p>
                                            </div>

                                              <button type="submit" class="btn btn-success">Edit</button>
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </form>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <!-- /.container-fluid -->
    </div>