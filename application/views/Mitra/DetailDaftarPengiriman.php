        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Pick Up</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Mitra Kurir</a></li>
                            <li><a href="<?php echo base_url();?>index.php/mitra/pickup">Halaman Pick Up</a></li>
                            <li class="active">Halaman Detail Pengiriman</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                                         <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-block btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#myModal">Tambah Data Pengiriman Eksternal</button>

                <!-- Modal -->
                <div class="modal fade" id="myModal">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="tf-ion-close"></i>
                  </button>
                    <div class="modal-dialog " role="document">
                      <div class="modal-content">
                         <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title">Pengajuan Penawaran Tarif Kerjasama</h3>
                        </div>
                          <div class="modal-body">
                            <form action="<?php echo base_url(). 'index.php/mitra/inputPengiriman'; ?>" method="post">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>No. Resi</span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" value="<?php echo substr(md5(time()), 0, 15);?>" name="no_resi" readonly>
                                        <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" value="<?php echo $id_kelompok_pengiriman;?>" name="id_kelompok_pengiriman">
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Nama Barang</span>
                                        <input style="font-size: 15px" type="text" class="form-control" name="nama_barang">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Jenis Pengiriman</span>
                                        <select name="jenis_pengiriman" class="form-control form-control-line" required>
                                            <option value="regular">Reguler</option>
                                            <option value="express">Ekspres</option>
                                        </select>
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Jenis Pengangkutan</span>
                                        <select name="jenis_pengangkutan" class="form-control form-control-line" required>
                                            <option value="kilo">kilo</option>
                                            <option value="koli">koli</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <span>Jenis Barang</span>
                                        <select name="jenis_barang" class="form-control form-control-line" required>
                                            <option value="Barang">Barang</option>
                                            <option value="Makanan">Makanan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <span>Berat <b style="color: red">*</b></span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="berat">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <span>Dimensi <b style="color: red">*</b></span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="dimensi">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <span>Total Unit <b style="color: red">*</b></span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="total_unit">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Kota Asal </span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="kota_asal">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Kota Tujuan</span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="kota_tujuan">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Alamat Tujuan </b></span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="alamat_tujuan">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Kecamatan </span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="kecamatan_tujuan">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Kelurahan </span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="kelurahan_tujuan">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Kode Pos </span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="kode_pos_tujuan">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Provinsi </span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="provinsi_tujuan">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Nama Pengirim </span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="nama_pengirim">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Nama Penerima </span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="nama_penerima">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>No Telp Pengirim </span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="no_telp_pengirim">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>No Telp Penerima </span>
                                        <input style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="no_telp_penerima">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <p style="color: red">*Kolom yang ditandai dengan bintang warna merah adalah boleh tidak di isi, sesuaikan dengan kebutuhan tawaran pengiriman anda.</p>
                            </div>

                              <button type="submit" class="btn btn-success">Submit</button>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </form>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- /Modal content-->
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Daftar Lokasi Pick Up Barang</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Cabang</th>
                                            <th>Alamat Cabang</th>
                                            <th>Berat</th>
                                            <th>Total Unit</th>
                                            <th>Pengirim</th>
                                            <th>Penerima</th>
                                            <th>No Telp Penerima</th>
                                            <th>Alamat Pengiriman</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    $no =0;
                                    foreach ($daftar_pengiriman as $key) {
                                    $no++;
                                    $tujuan = $key->alamat_tujuan.', '.$key->kecamatan_tujuan.', '.$key->kelurahan_tujuan.', '.$key->kode_pos_tujuan.', '.$key->kota_tujuan.'.';
                                    ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <?php if ($key->id_cabang != null and $key->id_perusahaan != null) { ?>
                                                <td><?php echo $key->nama_cabang;?></td>
                                                <td><?php echo $key->alamat;?></td>
                                            <?php }else{ ?>
                                                <td>-</td>
                                                <td>-</td>
                                            <?php } ?>
                                            <?php
                                            if ($key->jenis_pengangkutan == 'kilo') { ?>
                                                <td><?php echo $key->berat;?></td>
                                                <td><?php echo "-";?></td>
                                             <?php }else{ ?>
                                                <td><?php echo "-";?></td>
                                                <td><?php echo $key->total_unit;?></td>
                                             <?php } ?>
                                            <td><?php echo $key->nama_pengirim;?></td>
                                            <td><?php echo $key->nama_penerima;?></td>
                                            <td><?php echo $key->no_telp_penerima;?></td>
                                            <td><?php echo $tujuan;?></td>
                                        </tr>
                                        
                                    </tbody>
                                <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
