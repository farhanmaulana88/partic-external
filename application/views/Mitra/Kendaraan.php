    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-12">
                    <h4 class="page-title">Kendaraan</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Mitra Kurir</a></li>
                        <li class="active">Halaman Kendaraan</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- row -->
            <?php foreach ($kendaraan as $key) { ?>
            <div class="row">
                <div class="col-md-3 col-xs-12">
                                <label class="col-sm-12">Foto SIM</label>
                    <div class="white-box">
                        <div class="user-bg"> 
                            <center>             
                                    <img style="width: 290px; height: 230px; border: 4px solid #e84ab4; padding: 10px;" src="<?php echo base_url().'assets/FotoPendaftaran/'.$key->foto_sim;?>" alt="img">
                            </center>
                        </div>
                    </div>
                                <label class="col-sm-12">Foto STNK</label>
                    <div class="white-box">
                        <div class="user-bg"> 
                            <center>             
                                    <img style="width: 290px; height: 230px; border: 4px solid #e84ab4; padding: 10px;" src="<?php echo base_url().'assets/FotoPendaftaran/'.$key->foto_stnk;?>" alt="img">
                            </center>
                        </div>
                    </div>
                </div>

                <div class="col-md-8 col-xs-12">
                    <div class="white-box">
                        <form action="<?php echo base_url();?>index.php/mitra/updateKendaraan" class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-12">Jenis Kendaraan</label>
                                <div class="col-sm-12">
                                    <select class="form-control form-control-line" name="jenis_kendaraan">
                                        <option value="<?php echo $key->jenis_kendaraan;?>" selected><?php echo $key->jenis_kendaraan;?></option>
                                        <option value="mobil">Mobil</option>
                                        <option value="truck">Truck</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Nama Kendaraan</label>
                                <div class="col-md-12">
                                    <input type="text" name="nama_kendaraan" value="<?php echo $key->nama_kendaraan;?>" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Plat Kendaraan</label>
                                <div class="col-md-12">
                                    <input type="text" name="plat_kendaraan" value="<?php echo $key->plat_kendaraan;?>"  class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Tahun Kendaraan</label>
                                <div class="col-md-12">
                                    <input type="text" name="tahun_kendaraan" value="<?php echo $key->tahun_kendaraan;?>" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Domisili</label>
                                <div class="col-md-12">
                                    <input type="text" name="domisili" value="<?php echo $key->domisili;?>" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Bahan Bakar Kendaraan</label>
                                <div class="col-md-12">
                                        <select class="form-control form-control-line" name="id_bbm">
                                        <option value="<?php echo $key->id_bbm;?>" selected><?php echo $key->nama_bbm;?></option>
                                        <option value="1">Pertamax</option>
                                        <option value="2">Pertamax Turbo</option>
                                        <option value="3">Pertamax Dex</option>
                                        <option value="4">Premium</option>
                                        <option value="5">Pertalite</option>
                                        <option value="6">Dexlite</option>
                                        <option value="7">Bio Solar</option>
                                    </select>
                        
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                <label class="col-md-6">Foto SIM</label>
                                    <input type="file" name="foto_sim" class="form-control form-control-line"> 
                                </div>
                                <div class="col-md-6">
                                <label class="col-md-6">Foto STNK</label>
                                    <input type="file" name="foto_stnk" class="form-control form-control-line"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Status Ketersediaan</label>
                                <div class="col-sm-12">
                                    <select name="status_ketersediaan" class="form-control form-control-line">
                                        <option value="<?php echo $key->status_ketersediaan;?>" selected><?php echo $key->status_ketersediaan;?></option>
                                        <option value="Available">Available</option>
                                        <option value="On Trip">On Trip</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>