 <script 
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgP-WOSIf3GPWcFmPB1IXVMIZPiebhOno&callback=initMap">
    </script>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Pengiriman</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Mitra Kurir</a></li>
                            <li class="active">Halaman Pengiriman</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- Maps -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Maps Pengiriman Kurir</h3>
                            <div id="gmaps-simple" class="gmaps"></div>
                            <?php foreach ($finalResp as $key2) { ?>
                                <div hidden class="way-points-option"><input type="checkbox" checked name="way_points[]" class="way_points" value="<?php echo $key2->latitude;?>,<?php echo $key2->longitude;?>"> <?php echo $key2->alamat_tujuan;?></div>
                            <?php } ?>
                             <br>
                             Estimasi Waktu Tempuh: <?php echo number_format($totalWaktuAkhir,2);?> Menit
                             <br>
                             Estimasi Jarak Tempuh: <?php echo number_format($totalJarakAkhir,2);?> KM
                             <br>
                             Estimasi Biaya Bahan Bakar: Rp.<?php echo number_format($totalBiayaAkhir);?>
                        </div>
                    </div>
                </div>
        <!-- /maps -->
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Daftar Lokasi Pick Up Barang</h3>
                            <div class="table-responsive">
                                <table id="myTable" class="table">
                                    <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-success btn-block btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#updateStatus">Status Pengiriman</button>

            <button type="button" class="btn btn-info btn-block btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#myModal">Pemberitahuan Masalah Pengiriman</button>


                <!-- Modal -->
                <div class="modal fade" id="myModal">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="tf-ion-close"></i>
                  </button>
                    <div class="modal-dialog " role="document">
                      <div class="modal-content">
                         <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title">Pemberitahuan Masalah Pengiriman</h3>
                        </div>
                          <div class="modal-body">
                            <form action="<?php echo base_url(). 'index.php/mitra/inputReportMitra'; ?>" method="post">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <span>Jenis Masalah</span>
                                        <select name="jenis_masalah" class="form-control form-control-line" required>
                                            <option value="Telat">Telat</option>
                                            <option value="Masalah Kendaraan">Masalah Kendaraan</option>
                                            <option value="Kecelakaan">kecelakaan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Keterangan</span>
                                        <textarea style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="keterangan"></textarea>
                                        <textarea name="longlat" style="display: none" id="currentLonglat3" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>


                              <button type="submit" class="btn btn-success">Submit</button>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </form>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- /Modal content-->

                <!-- Modal -->
                <div class="modal fade" id="updateStatus">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="tf-ion-close"></i>
                  </button>
                    <div class="modal-dialog " role="document">
                      <div class="modal-content">
                         <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title">Update Status Pengiriman</h3>
                        </div>
                          <div class="modal-body">
                            <form action="<?php echo base_url(). 'index.php/mitra/inputStatusPengiriman'; ?>" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Keterangan</span>
                                        <textarea style="font-size: 15px" style="font-size: 15px" type="text" class="form-control" name="status"></textarea>
                                        <textarea name="longlat" style="display: none" id="currentLonglat4" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>


                              <button type="submit" class="btn btn-success">Submit</button>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </form>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- /Modal content-->


                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Alamat Pengiriman</th>
                                            <th>Pengirim</th>
                                            <th>Penerima</th>
                                            <th>No Telp Penerima</th>
                                            <th>Jenis Pengiriman</th>
                                            <th>Berat</th>
                                            <th>Total Unit</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <?php
                                    $no =0;
                                    foreach ($finalResp as $key) {
                                    $no++;
                                    $tujuan = $key->alamat_tujuan.', '.$key->kecamatan_tujuan.', '.$key->kelurahan_tujuan.', '.$key->kode_pos_tujuan.', '.$key->kota_tujuan.'.';
                                    ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $tujuan;?></td>
                                            <td><?php echo $key->nama_pengirim;?></td>
                                            <td><?php echo $key->nama_penerima;?></td>
                                            <td><?php echo $key->no_telp_penerima;?></td>
                                            <td><?php echo $key->jenis_pengiriman;?></td>
                                            <?php
                                            if ($key->jenis_pengangkutan == 'kilo') { ?>
                                                <td><?php echo $key->berat;?></td>
                                                <td><?php echo "-";?></td>
                                             <?php }else{ ?>
                                                <td><?php echo "-";?></td>
                                                <td><?php echo $key->total_unit;?></td>
                                             <?php } ?>
                                            <td>
                                                <a class="btn btn-info" data-toggle='modal' data-target='#modalStatusSampai<?= $key->id; ?>'><i class="ti-check-box fa-fw"></i></a>
                                            </td>
                                        </tr>
                                        
                                    </tbody>

                                  <!-- Modal Status Pengiriman = Sampai -->
                                     <div class="modal fade" id="modalStatusSampai<?php echo $key->id;?>">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="tf-ion-close"></i>
                              </button>
                                <div class="modal-dialog " role="document">
                                  <div class="modal-content">
                                     <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h3 class="modal-title">Penerima Barang Kiriman</h5>
                                                </div>
                                      <div class="modal-body">

                                            <form action="<?php echo base_url(). 'index.php/mitra/inputStatusPengirimanSampai'; ?>" method="post">

                                            <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="id" value="<?php echo $key->id;?>">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <span>Status</span>
                                                        <textarea name="status" style="font-size: 15px" style="font-size: 15px" type="text" class="form-control"></textarea>
                                                        <textarea name="longlat" style="display: none" id="currentLonglat2" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <p style="color: red">*Isi status dan nama penerima pada kolom apabila barang sudah sampai pada pelanggan, karena barang tidak akan tampil lagi di halaman pengiriman.</p>
                                            </div>

                                              <button type="submit" class="btn btn-success">Input</button>
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </form>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                  <!-- /Modal Update Status Pengiriman = Sampai -->

                                <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
    <script>
            var y = document.getElementById("currentLonglat2");
            var z = document.getElementById("currentLonglat3");
            var x1 = document.getElementById("currentLonglat4");
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
          } else {
            alert('error geo')
          }

        function showPosition(position) {
            y.innerHTML = position.coords.latitude + ',' + position.coords.longitude
            z.innerHTML = position.coords.latitude + ',' + position.coords.longitude
            x1.innerHTML = position.coords.latitude + ',' + position.coords.longitude
            var latlong = position.coords.latitude + ',' + position.coords.longitude
            // alert(latlong)
            initMap(latlong)
        }
        </script>

        <script>
        var map;
        var waypoints
        function initMap(latlong) {
                // alert(latlong)
                var mapLayer = document.getElementById("gmaps-simple"); 
                var centerCoordinates = new google.maps.LatLng(-6.914744, 107.609810);
                var defaultOptions = { center: centerCoordinates, zoom: 4 }
                // var trafficLayer = new google.maps.TrafficLayer();
                // trafficLayer.setMap(mapLayer);
                map = new google.maps.Map(mapLayer, defaultOptions);

    
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            directionsDisplay.setMap(map);

                // alert('aw')
                var origin = {location: latlong, stopover:true};
                    waypoints = Array(origin);
                    $('.way_points:checked').each(function() {
                    waypoints.push({
                        location: $(this).val(),
                        stopover: true
                    });
                    console.log(waypoints)
                    });
                var locationCount = waypoints.length;
                if(locationCount > 0) {
                    var start = waypoints[0].location;
                    var end = waypoints[locationCount-1].location;
                    drawPath(directionsService, directionsDisplay,start,end);
                }
            // });
            
        }
            function drawPath(directionsService, directionsDisplay,start,end) {
            directionsService.route({
              origin: start,
              destination: end,
              waypoints: waypoints,
              durationInTraffic: true,
              optimizeWaypoints: true,
              travelMode: 'DRIVING'
            }, function(response, status) {
                if (status === 'OK') {
                directionsDisplay.setDirections(response);
                } else {
                window.alert('Problem in showing direction due to ' + status);
                }
            });
      }
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgP-WOSIf3GPWcFmPB1IXVMIZPiebhOno&callback=initMap">
    </script>
    <script src="https://maps.google.com/maps/api/js?sensor=true"></script>


