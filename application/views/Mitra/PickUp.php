        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Kelompok Pengiriman</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Mitra Kurir</a></li>
                            <li class="active">Halaman Kelompok Pengiriman</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Daftar Kelompok Pengiriman Barang Barang</h3>
                            <div class="table-responsive">
                                <table id="myTable" class="table">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Perusahaan</th>
                                            <th>Asal</th>
                                            <th>Tujuan</th>
                                            <th>Jenis Pengangkutan</th>
                                            <th>Jenis Pengiriman</th>
                                            <th style="width: 40px"></th>
                                        </tr>
                                    </thead>
                                    <?php
                                    $no =0;
                                    foreach ($daftar_lobi_mitra as $key) {
                                    $no++;
                                    ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $key->nama_perusahaan;?></td>
                                            <td><?php echo $key->asal;?></td>
                                            <td><?php echo $key->tujuan;?></td>
                                            <td><?php echo $key->jenis_pengangkutan;?></td>
                                            <td><?php echo $key->jenis_pengiriman;?></td>
                                            <td>
                                                <a href="<?php echo base_url('index.php/mitra/detailDaftarPengiriman/'. $key->id_kelompok_pengiriman);?>" class="btn btn-primary"><i class="ti-search fa-fw"></i></a>
                                                <a href="<?php echo base_url('index.php/mitra/updateStatusPickUp/'. $key->id_kelompok_pengiriman);?>" class="btn btn-success"><i class="ti-pencil-alt fa-fw"></i></a>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
