    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-12">
                    <h4 class="page-title">Profil</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Mitra Kurir</a></li>
                        <li class="active">Halaman Profil</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-md-3 col-xs-12">
                                <label class="col-md-12">Foto Diri</label>
                    <div class="white-box">
                        <div class="user-bg"> 
                            <center>
                                    <img style="width: 220px;height: 220px; border: 4px solid #e84ab4; padding: 10px;" src="<?php echo base_url().'assets/FotoPendaftaran/'.$profil->DataBiodata->foto_diri;?>" class="thumb-lg img-circle" alt="img">
                            </center>
                        </div>
                    </div>
                                <label class="col-md-12">Foto KTP</label>
                    <div class="white-box">
                        <div class="user-bg"> 
                            <center>             
                                    <img style="width: 290px; height: 230px; border: 4px solid #e84ab4; padding: 10px;" src="<?php echo base_url().'assets/FotoPendaftaran/'.$profil->DataBiodata->foto_ktp;?>" alt="img">
                            </center>
                        </div>
                    </div>
                                <label class="col-md-12">Foto NPWP</label>
                    <div class="white-box">
                        <div class="user-bg"> 
                            <center>             
                                    <img style="width: 290px; height: 230px; border: 4px solid #e84ab4; padding: 10px;" src="<?php echo base_url().'assets/FotoPendaftaran/'.$profil->DataBiodata->foto_npwp;?>" alt="img">
                            </center>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="white-box">
                        <form action="<?php echo base_url(). 'index.php/mitra/updateProfil'; ?>" enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                            
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" value="<?php echo $profil->DataUser->email;?>" class="form-control form-control-line" name="email" id="example-email"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Nama Lengkap</label>
                                <div class="col-md-12">
                                    <input type="text" name="nama_lengkap" value="<?php echo $profil->DataBiodata->nama_lengkap;?>" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Tanggal Lahir</label>
                                <div class="col-md-12">
                                    <input type="date" name="tanggal_lahir" value="<?php echo $profil->DataBiodata->tanggal_lahir;?>" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Tempat Lahir</label>
                                <div class="col-md-12">
                                    <input type="text" name="tempat_lahir" value="<?php echo $profil->DataBiodata->tempat_lahir;?>" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Alamat</label>
                                <div class="col-md-12">
                                    <textarea rows="5" name="alamat" class="form-control form-control-line"><?php echo $profil->DataBiodata->alamat;?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Nomer Telpon</label>
                                <div class="col-md-12">
                                    <input type="text" name="no_telp" value="<?php echo $profil->DataBiodata->no_telp;?>" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Nik</label>
                                <div class="col-md-12">
                                    <input type="text" name="nik" value="<?php echo $profil->DataBiodata->nik;?>" class="form-control form-control-line"> </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label class="col-md-6">Foto Diri</label>
                                    <input type="file" name="foto_diri" class="form-control form-control-line"> 
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-6">Foto KTP</label>
                                    <input type="file" name="foto_ktp" class="form-control form-control-line"> 
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-6">Foto NPWP</label>
                                    <input type="file" name="foto_npwp" class="form-control form-control-line"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>