
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Riwayat Pengiriman</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Mitra Kurir</a></li>
                            <li class="active">Halaman Riwayat Pengiriman</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Daftar Barang Yang Telah Dikirim</h3>
                            <div class="table-responsive">
                                <table id="myTable" class="table">
                                    Jumlah Pendapatan : <?php echo $sum;?>
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Perusahaan</th>
                                            <th>Pengiriman</th>
                                            <th>Pengirim</th>
                                            <th>Penerima</th>
                                            <th>No Telp Penerima</th>
                                            <th>Alamat Pengiriman</th>
                                            <th>Tanggal PickUp</th>
                                            <th>Tanggal Sampai</th>
                                            <th>Hari Telat</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <?php
                                    $no =0;
                                    foreach ($riwayat2 as $key) {
                                        // print_r($key);die;
                                    $no++;
                                    $tujuan = $key->pengiriman->alamat_tujuan.', '.$key->pengiriman->kecamatan_tujuan.', '.$key->pengiriman->kelurahan_tujuan.', '.$key->pengiriman->kode_pos_tujuan.', '.$key->pengiriman->kota_tujuan.'.';
                                    ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $key->pengiriman->nama_perusahaan;?></td>
                                            <td><?php echo $key->pengiriman->jenis_pengiriman;?></td>
                                            <td><?php echo $key->pengiriman->nama_pengirim;?></td>
                                            <td><?php echo $key->pengiriman->nama_penerima;?></td>
                                            <td><?php echo $key->pengiriman->no_telp_penerima;?></td>
                                            <td><?php echo $tujuan;?></td>
                                            <td><?php echo $key->pengiriman->tanggal_pickup;?></td>
                                            <td><?php echo $key->pengiriman->tanggal_sampai;?></td>
                                            <td><?php echo $key->pengiriman->jumlah_hari_telat;?></td>
                                            <!-- <td><?php echo $key->pengiriman->tarif_normal;?></td> -->
                                            <td>
                                                <a class="btn btn-info" data-toggle='modal' data-target='#modalDetail<?= $key->pengiriman->id_pengiriman; ?>'><i class="ti-info fa-fw"></i></a>
                                            </td>
                                        </tr>
                                        
                                    </tbody>

                                    <!-- Modal Detail Pengiriman -->
                                     <div class="modal fade" id="modalDetail<?php echo $key->pengiriman->id_pengiriman;?>">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="tf-ion-close"></i>
                              </button>
                                <div class="modal-dialog " role="document">
                                  <div class="modal-content">
                                     <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h3 class="modal-title">Detail Pengiriman</h5>
                                                </div>
                                      <div class="modal-body">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <span>Nomer Resi</span>
                                                        <input type="text" name="status" style="font-size: 15px" value="<?php echo $key->pengiriman->no_resi;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Nama Perusahaan</span>
                                                        <input type="text" name="status" style="font-size: 15px" value="<?php echo $key->pengiriman->nama_perusahaan;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Nama Cabang</span>
                                                        <input name="status" style="font-size: 15px" type="text" value="<?php echo $key->pengiriman->nama_cabang;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <span>Alamat Cabang</span>
                                                        <textarea name="status" style="font-size: 15px" type="text" class="form-control" readonly><?php echo $key->pengiriman->alamat;?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Jenis Pengiriman</span>
                                                        <input type="text" name="status" style="font-size: 15px" value="<?php echo $key->pengiriman->jenis_pengiriman;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Tarif</span>
                                                        <input name="status" style="font-size: 15px" type="text" value="<?php echo $key->tarif;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Nama Pengirim</span>
                                                        <input type="text" name="status" style="font-size: 15px" value="<?php echo $key->pengiriman->nama_pengirim;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Nama penerima</span>
                                                        <input name="status" style="font-size: 15px" type="text" value="<?php echo $key->pengiriman->nama_penerima;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>No Telp Pengirim</span>
                                                        <input type="text" name="status" style="font-size: 15px" value="<?php echo $key->pengiriman->no_telp_pengirim;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>No Telp Penerima</span>
                                                        <input name="status" style="font-size: 15px" type="text" value="<?php echo $key->pengiriman->no_telp_penerima;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <span>Alamat Tujuan</span>
                                                        <textarea name="status" style="font-size: 15px" type="text" class="form-control" readonly><?php echo $key->pengiriman->alamat_tujuan;?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Tanggal Pick Up</span>
                                                        <input type="text" name="status" style="font-size: 15px" value="<?php echo $key->pengiriman->tanggal_pickup;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span>Tanggal Sampai</span>
                                                        <input name="status" style="font-size: 15px" type="text" value="<?php echo $key->pengiriman->tanggal_sampai;?>" class="form-control" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </form>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                  <!-- /Modal Detail Pengiriman -->

                                <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>

